package es.kibu.geoapis.metrics.sdk.data.providers;

import android.support.annotation.NonNull;

import org.junit.Test;

import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.data.providers.DataProvider;
import es.kibu.geoapis.objectmodel.Dimension;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;

import static org.junit.Assert.*;

/**
 * Created by lrodriguez2002cu on 27/03/2017.
 */
public class AndroidTimeIdContextAwareProviderTest {

    @Test
    public void testAndroidTimeIdContextAwareProvider() {
        DataProvider<String>  provider = new AndroidTimeIdContextAwareProvider();

        DataContext dataContextTimeId = getDataContextTimeId("varName");
        DataContext dataContextPrevId = getDataContextPrevForVar("varName");

        String timeIdVal = provider.getNext(dataContextTimeId);
        String prevIdVal = provider.getNext(dataContextPrevId);
        String newtimeIdVal = provider.getNext(dataContextTimeId);
        String newPrevIdVal = provider.getNext(dataContextPrevId);
        String newtimeIdVal1 = provider.getNext(dataContextTimeId);
        String newPrevIdVal1 = provider.getNext(dataContextPrevId);

        assertTrue(prevIdVal == null);
        assertTrue(newPrevIdVal.equalsIgnoreCase(timeIdVal));
        assertTrue(newPrevIdVal1.equalsIgnoreCase(newtimeIdVal));

    }

    @Test
    public void testTwoUnrelatedContexts() {
        DataProvider<String>  provider = new AndroidTimeIdContextAwareProvider();

        final String varName1 = "varName1";
        String varName = "varName2";

        DataContext dataContextTimeIdV1 = getDataContextTimeId(varName1);
        DataContext dataContextPrevIdV1 = getDataContextPrevForVar(varName1);

        DataContext dataContextTimeIdV2 = getDataContextTimeId(varName);
        DataContext dataContextPrevIdV2 = getDataContextPrevForVar(varName);

        String timeIdVal1 = provider.getNext(dataContextTimeIdV1);
        String timeIdVal2 = provider.getNext(dataContextTimeIdV2);

        String prevIdVal1 = provider.getNext(dataContextPrevIdV1);
        String prevIdVal2 = provider.getNext(dataContextPrevIdV2);

        String newtimeIdVal1 = provider.getNext(dataContextTimeIdV1);
        String newtimeIdVal2 = provider.getNext(dataContextTimeIdV2);

        String newprevIdVal1 = provider.getNext(dataContextPrevIdV1);
        String newprevIdVal2 = provider.getNext(dataContextPrevIdV2);


        assertTrue(prevIdVal1 == null);
        assertTrue(prevIdVal2 == null);

        assertTrue(newprevIdVal1.equalsIgnoreCase(timeIdVal1));
        assertTrue(newprevIdVal2.equalsIgnoreCase(timeIdVal2));



    }

    @NonNull
    private DataContext getDataContextTimeId(final String varName1) {
        return new DataContext() {
                @Override
                public MetricsDefinition getDefinition() {
                    MetricsDefinition metricsDefinition = new MetricsDefinition();
                    metricsDefinition.setId("sampleId");
                    return metricsDefinition;
                }

                @Override
                public Variable getVariable() {
                    Variable variable = new Variable();
                    variable.setName(varName1);
                    return variable;
                }

                @Override
                public Dimension getDimension() {
                    return new Dimension() {
                        @Override
                        public String getName() {
                            return "timeid";
                        }
                    };
                }
            };
    }

    @NonNull
    private DataContext getDataContextPrevForVar(final String varName) {
        return new DataContext() {
                @Override
                public MetricsDefinition getDefinition() {
                    MetricsDefinition metricsDefinition = new MetricsDefinition();
                    metricsDefinition.setId("sampleId");
                    return metricsDefinition;
                }

                @Override
                public Variable getVariable() {
                    Variable variable = new Variable();
                    variable.setName(varName);
                    return variable;
                }

                @Override
                public Dimension getDimension() {
                    return new Dimension() {
                        @Override
                        public String getName() {
                            return "previd";
                        }
                    };
                }
            };
    }

}