package es.kibu.geoapis.metrics.sdk.data.providers;

import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.junit.Test;

import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.serialization.GsonUtils;

import static org.junit.Assert.*;

/**
 * Created by lrodriguez2002cu on 12/09/2017.
 */
public class AndroidLocationProviderTest {

    @Test
    public void testTimeSerialization() {
        Gson gson = GsonUtils.getGson();
        for (int i =0 ;  i< 1000; i++) {
            String timeStr = gson.toJson(DateTime.now());
            System.out.println("Serialized as: " +  timeStr);
        }
    }

    @Test
    public void testLocationSerialization() {
        Gson gson = GsonUtils.getGson();
        for (int i =0 ;  i< 1000; i++) {
            DateTime now = DateTime.now();
            DataUtils.Location loc = new DataUtils.Location(Math.random(), Math.random(), Math.random(), Math.random(), now);
            String logStr = gson.toJson(loc);
            System.out.println("Serialized as: " +  logStr);
        }
    }


}