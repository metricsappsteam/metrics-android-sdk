package es.kibu.geoapis.metrics.sdk.data.providers;

import com.google.gson.JsonElement;

import org.junit.Test;

import es.kibu.geoapis.data.DataUtils;

import static org.junit.Assert.*;

/**
 * Created by lrodriguez2002cu on 04/04/2017.
 */
public class SubmissionDataEventTest {


    @Test
    public void testReadingFromRetry(){

        String json =
                "{\n" +
                "\t\"timeid\": \"sampletimeid\",\n" +
                "\t\"location\": {\n" +
                "\t\t\"latitude\": 39.004,\n" +
                "\t\t\"longitude\": 0.004,\n" +
                "\t\t\"altitude\": 10,\n" +
                "\t\t\"accuracy\": 0.5,\n" +
                "\t\t\"time\": \"2016-04-12T07:44:15.000Z\" \n" +
                "\t}, \n" +
                "    \"steps\": 1000,\n" +
                "    \"previd\": \"sampleprevid\",\n" +
                "    \"time\":\"sample time\"\n" +
                "}" ;

        //RetryData data = new RetryData(json, "application", "movement");

        AndroidDataSubmitter.ContextDetails details = new AndroidDataSubmitter.ContextDetails("application1", "variable1");
        AndroidDataSubmitter.SubmissionDataEvent event = new AndroidDataSubmitter.SubmissionDataEvent(AndroidDataSubmitter.SubmissionStage.RETRIED, details, json);

        DataUtils.Location location = event.getLocation();

        assertTrue(location.latitude == 39.004);
        assertTrue(location.longitude == 0.004);

        assertTrue(event.getProperties().size() == 5);
        assertTrue(event.getValue("steps") instanceof JsonElement);
        assertTrue(((JsonElement)event.getValue("steps")).getAsInt() == 1000);


    }

}