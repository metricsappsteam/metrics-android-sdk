package es.kibu.geoapis.metrics.sdk.api.events;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by lrodriguez2002cu on 06/04/2017.
 */
public class BaseMetricsEventTest {

    String sampleEvent = "{\n" +
            "     \"value\": {\n" +
            "     \"result\": true,\n" +
            "     \"json\": \"\\\"payload\\\"\"\n" +
            "     },\n" +
            "     \"time\": \"2017-04-06T06:35:41.726Z\",\n" +
            "     \"application\": \"app-45519f89e1bb4c4b\",\n" +
            "     \"user\": \"user1\",\n" +
            "     \"session\": \"session1\",\n" +
            "     \"execid\": \"40859360-1a93-11e7-8c9c-0242ac120004\",\n" +
            "     \"timeid\": \"415da3e0-1a93-11e7-8eb2-9b6442a6432d\"\n" +
            "     }";

    @Test
    public void testSerialization() {
        ApplicationEvent event = new ApplicationEvent(sampleEvent);

        assertTrue(event.application.equalsIgnoreCase("app-45519f89e1bb4c4b"));
        assertTrue(event.session.equals("session1"));
        assertTrue(event.user.equals("user1"));
        assertTrue(event.time.equals(new DateTime("2017-04-06T06:35:41.726Z")));
        assertTrue(event.execid.equals("40859360-1a93-11e7-8c9c-0242ac120004"));
        assertTrue(event.timeid.equals("415da3e0-1a93-11e7-8eb2-9b6442a6432d"));
    }

}