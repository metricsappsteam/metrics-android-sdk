package es.kibu.geoapis.metrics.sdk.data.services;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.firebase.FirebaseApp;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.BasicMetricsDefinitionReader;
import es.kibu.geoapis.services.objectmodel.results.CreateResult;
import es.kibu.geoapis.services.objectmodel.results.DeleteResult;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;
import es.kibu.geoapis.services.objectmodel.results.ResultValue;
import es.kibu.geoapis.services.objectmodel.results.UpdateResult;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by lrodriguez2002cu on 04/01/2017.
 */
public class DefaultAsyncServiceClientTest extends BaseServicesTest {

    public static final int TIMEOUT = 300;
    public static final String SAMPLE_METRICS_JSON = "sample_metric.json";


    MetricsAsyncServiceClient client;
    public static final String BASE_URL = "http://altairi.lan:9000";


    public DefaultAsyncServiceClientTest() {
        /*Context ctx =
        client = new DefaultMetricsAsyncServiceClient(BASE_URL, ctx);*/

    }

    @Override
    protected InputStream getMetricsDefinitionStream(String filename) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        InputStream inputStream = null;
        try {
            inputStream = targetContext.getResources().getAssets().open(SAMPLE_METRICS_JSON);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return inputStream;
    }

    @Before
    public void getServiceClient() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        FirebaseApp.initializeApp(appContext);
        client = new DefaultMetricsAsyncServiceClient(BASE_URL, appContext);
    }

    class TestingAsyncResult<Result> extends AsyncResult<Result> {

        final CountDownLatch signal = new CountDownLatch(1);

        public TestingAsyncResult() {
            super(null);
        }

        public TestingAsyncResult(ProgressHandler handler) {
            super(handler);
        }

        @Override
        public void onFailure(Exception ex) {
            Log.d("CLIENT_ERROR", (ex.getMessage() != null) ? ex.getMessage() : ex.getClass().getName());
            signal.countDown();
        }

        @Override
        public void onSuccess(Result result) {
            Log.d("CLIENT_SUCCESS", result.toString());
            signal.countDown();
        }

        public void await(long timeout) {
            try {
                signal.await(timeout, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }


    private void checkForException(AsyncResult result) {
        if (result.isFailed()) {
            Log.e("CHECK_FOR_EXCEPTION", "Error found" + result.getFailureException().getMessage(), result.getFailureException());
            if (result.getFailureException() instanceof Exception) {
                throw new RuntimeException((result.getFailureException()));
            }
            throw new RuntimeException(result.getFailureException());
        }
    }

    @Test
    public void testDeleteComplex() throws Exception {
        String applicationId = getRandomAppId();
        checkCreateMetric(applicationId, 200);
        checkDeleteMetric(applicationId, 200, "deleted");
    }

    @Test
    public void testUpdateMetric() throws Exception {
        super.testUpdateMetric();
    }

    @Test
    public void testGetMetric() throws Exception {
        //String applicationId = "sample-appid";
        super.testGetMetric();
    }

    @Override
    public void gettingAMetricForANonExistingAppShouldFail() throws IOException {
        try {
            super.gettingAMetricForANonExistingAppShouldFail();
            fail();
        } catch (Exception e) {

            if (e.getCause() instanceof DefaultResponseDeserializer.ServiceException) {
                assertTrue(e.getMessage().contains("No metrics"));
            } else fail("expected cause must be a ServiceException");

        }
    }

    @Override
    public void creatingAMetricForTheSameAppTwiceShouldFail() throws Exception {
        try {
            super.creatingAMetricForTheSameAppTwiceShouldFail();
            fail();
        } catch (Exception e) {

            if (e.getCause() instanceof DefaultResponseDeserializer.ServiceException) {
                assertTrue(e.getMessage().contains("already exist"));
            } else fail("expected cause must be a ServiceException");

        }
    }

    @Override
    public void updatingAMetricForNonExistignAppShouldFail() throws Exception {
        try {
            super.updatingAMetricForNonExistignAppShouldFail();
            fail();
        } catch (Exception e) {

            if (e.getCause() instanceof DefaultResponseDeserializer.ServiceException) {
                assertTrue(e.getMessage().contains("not exist"));
            } else fail("expected cause must be a ServiceException");
        }
    }

    @Override
    public void testCreateMetric() throws Exception {
        super.testCreateMetric();
    }


    @Override
    protected String getCreateResult(String applicationId, JsonNode jsonNode, int expectedCode) {
        TestingAsyncResult<CreateResult> result = new TestingAsyncResult<>();
        //BasicMetricsDefinitionWriter writer = getWriter();
        BasicMetricsDefinitionReader reader = getReader();
        MetricsDefinition metricsDefinition = reader.readFrom(jsonNode.toString());
        client.createMetricAsync(applicationId, metricsDefinition, result);
        result.await(TIMEOUT);
        checkForException(result);
        try {
            return getResultJson(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    protected String getUpdateResult(String applicationId, JsonNode jsonNode, int expectedCode) {
        TestingAsyncResult<UpdateResult> result = new TestingAsyncResult<>();
        BasicMetricsDefinitionReader reader = getReader();
        MetricsDefinition metricsDefinition = reader.readFrom(jsonNode.toString());
        client.updateMetricsAsync(applicationId, metricsDefinition, result);
        result.await(TIMEOUT);
        checkForException(result);
        try {
            return getResultJson(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    protected String getMetricsExistResult(String applicationId, int expectedCode) {

        TestingAsyncResult<ExistResult> result = new TestingAsyncResult<>();
        client.metricsExistAsync(applicationId, result);
        result.await(TIMEOUT);
        checkForException(result);
        try {
            return getResultJson(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    protected String getGetMetricResult(String applicationId, int expectedCode) {

        TestingAsyncResult<MetricsResult> result = new TestingAsyncResult<>();
        client.metricsAsync(applicationId, result);
        result.await(TIMEOUT);
        checkForException(result);
        try {
            return getResultJson(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    private String getResultJson(TestingAsyncResult<?> result) throws JsonProcessingException {
        ResultValue value = new ResultValue(result.getResult());
        String json = Json.toJson(value);
        Log.d("RETURNED_JSON", json);
        return json;
    }

    @Override
    protected String getDeleteMetricResult(String applicationId, int expectedCode) {

        TestingAsyncResult<DeleteResult> result = new TestingAsyncResult<>();
        client.deleteMetricsAsync(applicationId, result);
        result.await(TIMEOUT);
        checkForException(result);
        try {
            return getResultJson(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


}