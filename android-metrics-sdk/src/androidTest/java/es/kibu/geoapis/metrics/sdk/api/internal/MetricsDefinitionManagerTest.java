package es.kibu.geoapis.metrics.sdk.api.internal;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.google.firebase.FirebaseApp;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import es.kibu.geoapis.metrics.sdk.api.MetricsConfig;
import es.kibu.geoapis.metrics.sdk.data.history.RetryData;
import es.kibu.geoapis.metrics.sdk.data.services.MetricsAsyncServiceClient;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

import static es.kibu.geoapis.metrics.sdk.data.services.DefaultAsyncServiceClientTest.SAMPLE_METRICS_JSON;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by lrodriguez2002cu on 11/01/2017.
 */

public class MetricsDefinitionManagerTest {

    String application = "app1";
    DefinitionManagerStore store;
    MetricsAsyncServiceClient client;

    @BeforeClass
    public static void init() {

        Context context = InstrumentationRegistry.getTargetContext();

        FirebaseApp.initializeApp(context);

        Configuration configuration = new Configuration.Builder(context)
                .addModelClass(RetryData.class)
                .addModelClass(es.kibu.geoapis.metrics.sdk.api.internal.DefinitionResultRecord.class)
                .addTypeSerializer(es.kibu.geoapis.metrics.sdk.data.history.ActiveAndroidSerializers.DateTimeSerializer.class)
                .setDatabaseName("test.db")
                .create();

        ActiveAndroid.initialize(configuration, true);
    }

    @Before
    public void setUp() throws Exception {
        store = DefaultDefinitionManagerStore.getStore();
    }

    @Test
    public void testDelete() throws Exception {

        MetricsResult data = new MetricsResult("sampleContent", "sampleHash", "sampleMetricsId", true);
        store.saveOrUpdateMetricsResults(data, application);

        boolean exists = store.existsForApp(application);
        assertTrue(exists);

        store.deleteForApp(application);

        exists = store.existsForApp(application);
        assertTrue(!exists);

        List<MetricsResult> metricsResults = store.loadForApp(application);
        assertTrue(metricsResults.size() == 0);

    }

    @Test
    public void testStorageExists() {
        String randomApp = "app" + DateTime.now().getMillis();
        boolean exists = store.existsForApp(randomApp);
        assertTrue(!exists);
    }

    @Test
    public void testStorageSaveOrUpdate() {
        String application = "app1" + DateTime.now().getMillis();

        MetricsResult data = new MetricsResult("sampleContent", "sampleHash", "sampleMetricsId", true);
        store.saveOrUpdateMetricsResults(data, application);

        boolean exists = store.existsForApp(application);
        assertTrue(exists);

        List<MetricsResult> metricsResults = store.loadForApp(application);
        assertTrue(metricsResults.size() == 1);

        MetricsResult metricsResult = metricsResults.get(0);
        assertTrue(metricsResult.equals(data));

    }

    @Test
    public void getInstance() throws Exception {

        try {
            MetricsDefinitionManager.getInstance();
            fail();
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("init"));
            //init must be called first
        }

        doInit();

    }

    private MetricsDefinitionManager doInit() {

        boolean clear = true;

        MetricsConfig config = new MetricsConfig() {
            @Override
            public String getUser() {
                return "user";
            }

            @Override
            public String getApplication() {
                return "application1";
            }

            @Override
            public String getSession() {
                return "session1";
            }
        };

        MetricsDefinitionManager definitionManager =
                MetricsDefinitionManager.init(config, InstrumentationRegistry.getTargetContext(), client, null /*null*/);

        if (clear) {
            definitionManager.clearAll();
        }

        return definitionManager;
    }


    @Test
    public void getMetricsDefMap() throws Exception {
        doInit();
        assertTrue(MetricsDefinitionManager.getInstance().getMetricsDefMap().size() == 0);
    }


    protected InputStream getMetricsDefinitionStream(String filename) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        InputStream inputStream = null;
        try {
            inputStream = targetContext.getResources().getAssets().open(filename);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return inputStream;
    }


    @Test
    public void getMetricsDefinition() throws Exception {

        InputStream metricsDefinitionStream = getMetricsDefinitionStream(SAMPLE_METRICS_JSON);

        String content = IOUtils.toString(metricsDefinitionStream, "UTF-8");

        assertTrue(content != null);

        doInit();
        assertTrue(MetricsDefinitionManager.getInstance().getMetricsDefMap().size() == 0);

        String application = "app1" + DateTime.now().getMillis();

        MetricsResult data = new MetricsResult(content, "sampleHash", "sampleMetricsId", true);
        store.saveOrUpdateMetricsResults(data, application);


        MetricsDefinitionManager.getInstance().reload();
        MetricsDefinition metricsDefinition = MetricsDefinitionManager.getInstance().getMetricsDefinition(application);

        assertTrue(metricsDefinition != null);

    }

}