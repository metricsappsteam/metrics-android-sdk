package es.kibu.geoapis.metrics.sdk.api;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.kibu.geoapis.api.Tracker;
import es.kibu.geoapis.data.providers.DummyTemperatureProvider;
import es.kibu.geoapis.metrics.sdk.data.history.RetryData;

//import org.mockito.Mock;

/**
 * Created by lrodriguez2002cu on 08/11/2016.
 */
@RunWith(AndroidJUnit4.class)
public class AndroidMetricsApiTest extends BaseTrackerTest {

    @BeforeClass
    public static void setUp() throws Exception {
        Context context = InstrumentationRegistry.getTargetContext();
        AndroidMetricsApi.setTesting(true);

        Configuration configuration = new Configuration.Builder(context)
                .addModelClass(RetryData.class)
                .addModelClass(es.kibu.geoapis.metrics.sdk.api.internal.DefinitionResultRecord.class)
                .addTypeSerializer(es.kibu.geoapis.metrics.sdk.data.history.ActiveAndroidSerializers.DateTimeSerializer.class)
                .setDatabaseName("testdb1.db")
                .create();
        ActiveAndroid.initialize(configuration, true);

    }

    @Override
    public Tracker createTracker() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        MetricsConfig config = new MetricsConfig() {
            @Override
            public String getUser() {
                return "user1";
            }

            @Override
            public String getApplication() {
                return "application1";
            }

            @Override
            public String getSession() {
                return "session1";
            }
        };

        AndroidMetricsApi.init(appContext, config);
        AndroidMetricsApi api = (AndroidMetricsApi)AndroidMetricsApi.getInstance();

        Tracker tracker = api.getDefaultTracker(true);
        return tracker;
    }


/*    protected Tracker createTrackerStrict() {

       *//* TrackerFactory factory = DaggerStrictTrackerFactory.builder().build();
        //DaggerTrackerFactory.builder().providersModule().
        Tracker tracker = factory.createTracker();
        return tracker;*//*

        RegistryMaker registryMaker = DaggerRegistryMaker.create();
        SchemaValidationStrictBaseTracker tracker =  new SchemaValidationStrictBaseTracker();
        tracker.setRegistry(registryMaker.createRegistry());

        return tracker;
    }*/


    @Test
    public void testCheckClassLoader() {
        try {
            Class<?> aClass = DummyTemperatureProvider.class.getClassLoader().loadClass(/*"es.kibu.geoapis.data.providers.DummyTemperatureProvider"*/
                    DummyTemperatureProvider.class.getCanonicalName());
            Log.d("CLASS_LOADER", aClass.getCanonicalName().toString());

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


}