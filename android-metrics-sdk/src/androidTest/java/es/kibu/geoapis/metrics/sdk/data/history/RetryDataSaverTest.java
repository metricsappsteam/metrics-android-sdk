package es.kibu.geoapis.metrics.sdk.data.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.google.gson.Gson;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by lrodriguez2002cu on 03/01/2017.
 */

@RunWith(AndroidJUnit4.class)
public class RetryDataSaverTest {

    @BeforeClass
    public static void init() {
        Context context = InstrumentationRegistry.getTargetContext();
        Configuration configuration = new Configuration.Builder(context)
                .addModelClass(RetryData.class)
                .addModelClass(es.kibu.geoapis.metrics.sdk.api.internal.DefinitionResultRecord.class)
                .addTypeSerializer(es.kibu.geoapis.metrics.sdk.data.history.ActiveAndroidSerializers.DateTimeSerializer.class)
                .setDatabaseName("testdb1.db")
                .create();


        ActiveAndroid.initialize(configuration, true);
        //ActiveAndroid.getDatabase().execSQL();
    }

    public static class SampleTestConstants {
        public static final String RESULT1 = "RESULT1";
        public static final String RESULT2 = "RESULT2";
        public static final String RESULT3 = "RESULT3";
    }

    private <T> T getTestData(String json) {
        Gson gson = new Gson();
        return (T) gson.fromJson(json, String.class);
    }

    @Test
    public void testSaveToHistory() {

        clearHistoryResults();
        String application = getApplication();
        String variable = getVariable();

        long randTests = Math.round(Math.random() * 10);

        int synczedCount = 0;
        int unsynczedCount = 0;

        if (randTests > 0) {

            for (long i = 0; i < randTests; i++) {
                boolean synczed = i % 2 == 0;
                String json = SampleTestConstants.RESULT2;
                String answers = getTestData(json);//RetryDataSaver.testAnswersFromString(SampleTestConstants.RESULT3);
                RetryDataSaver.saveResults(answers,application, variable, synczed);
                if (synczed) {
                    synczedCount++;
                } else {
                    unsynczedCount++;
                }
            }

        }

        Map<RetryData, String> unsynczedAnswers = RetryDataSaver.loadAllUnSynczed();
        assertEquals(unsynczedCount, unsynczedAnswers.size());

        Map<RetryData, String> synczedAnswers = RetryDataSaver.loadAllSynczed();
        assertEquals(synczedCount, synczedAnswers.size());

        Map<RetryData, String> allAnswers = RetryDataSaver.loadAll();
        assertEquals(synczedCount + unsynczedCount, allAnswers.size());


    }

    @NonNull
    private String getApplication() {
        return "app1";
    }

    @Test
    public void testHistoryOperations() {
        clearHistoryResults();

        String application = getApplication();
        String variable = getVariable();

        String json = SampleTestConstants.RESULT3;
        String answers = getTestData(json);
        boolean synczed = true;

        RetryDataSaver.saveResults(answers, application, variable, synczed);
        assertTrue(RetryDataSaver.loadAll().size() == 1);

        clearHistoryResults();
        assertTrue(RetryDataSaver.loadAll().size() == 0);

        json = SampleTestConstants.RESULT3;
        answers = getTestData(json);

        synczed = true;
        RetryDataSaver.saveResults(answers, application, variable, synczed);
        assertTrue(RetryDataSaver.loadAllSynczed().size() == 1);
        assertTrue(RetryDataSaver.loadAllUnSynczed().size() == 0);
        assertTrue(RetryDataSaver.loadAll().size() == 1);

        clearHistoryResults();

    }

    @Test
    public void testSaveToHistoryWithUpdateSoThatAllBecomeSynczed() {

        clearHistoryResults();
        String application = getApplication();
        String variable = getVariable();
        long randTests = Math.round(Math.random() * 10);

        int synczedCount = 0;
        int unsynczedCount = 0;

        if (randTests > 0) {

            for (long i = 0; i < randTests; i++) {
                boolean synczed = i % 2 == 0;
                String json = synczed ? SampleTestConstants.RESULT2 : SampleTestConstants.RESULT3;
                String answers = getTestData(json);//RetryDataSaver.testAnswersFromString(SampleTestConstants.RESULT3);
                RetryDataSaver.saveResults(answers, application, variable, synczed);
                if (synczed) {
                    synczedCount++;
                } else {
                    unsynczedCount++;
                }
            }

            /*json = SampleTestConstants.RESULT3;
            answers = getTestData(json);//RetryDataSaver.testAnswersFromString(SampleTestConstants.RESULT3);
            RetryDataSaver.saveResults(answers, synczed);*/
        }

        Map<RetryData, String> unsynczedAnswers = RetryDataSaver.loadAllUnSynczed();
        assertEquals(unsynczedCount, unsynczedAnswers.size());

        Map<RetryData, String> synczedAnswers = RetryDataSaver.loadAllSynczed();
        assertEquals(synczedCount, synczedAnswers.size());

        Map<RetryData, String> allAnswers = RetryDataSaver.loadAll();
        assertEquals(synczedCount + unsynczedCount, allAnswers.size());


        //make the unsinczed sinczed
        for (Map.Entry<RetryData, String> unSyncedAnswer : unsynczedAnswers.entrySet()) {
            final RetryData historyResult = unSyncedAnswer.getKey();
            RetryDataSaver.updateHistory(historyResult, true);

        }

        //check that now all of them are synchronized
        synczedAnswers = RetryDataSaver.loadAllSynczed();
        assertEquals(synczedCount + unsynczedCount, synczedAnswers.size());

        //and eventually all of them are ...
        allAnswers = RetryDataSaver.loadAll();
        assertEquals(synczedCount + unsynczedCount, allAnswers.size());

        //more over none of them are not in sync
        unsynczedAnswers = RetryDataSaver.loadAllUnSynczed();
        assertEquals(0, unsynczedAnswers.size());
    }

    @NonNull
    private String getVariable() {
        return "var1";
    }

    private void clearHistoryResults() {
        RetryDataSaver.clearHistoryResults();
    }

}


