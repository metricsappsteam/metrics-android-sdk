package es.kibu.geoapis.metrics.sdk.api.internal;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
/*import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;*/

import es.kibu.geoapis.metrics.sdk.data.services.AsyncResult;
import es.kibu.geoapis.metrics.sdk.data.services.MetricsAsyncServiceClient;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

//import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by lrodriguez2002cu on 04/09/2017.
 */
//@RunWith(MockitoJUnitRunner.class)
public class MetricsDefinitionManagerRecoveryTests {

   /* @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    MetricsDefinitionManager.MetricsDefinitionResolver resolver;

    @Mock
    MetricsAsyncServiceClient client1;

    @InjectMocks
    private MetricsDefinitionManager manager1;
*/
    @Test
    public void getMetricsDefinition1() throws Exception {

        // Mockito.doThrow(new Exception()).when(resolver).solve(anyString());
       /* Mockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                AsyncResult<MetricsResult> resultAsyncResult = (AsyncResult<MetricsResult>) args[0];
                resultAsyncResult.onFailure(new RuntimeException("Problems..with definition"));
                return null;
            }
        }).when(resolver).solve(anyString());

        MDEvents events = new MDEvents();

        manager1.registerEventHandler(events);
        manager1.retryReload(5000);

        Assert.assertTrue(events.getCountFailures() == 1);
        Assert.assertTrue(events.getCountSuccess() == 0);
*/
    }

    class MDEvents implements MetricsDefinitionManager.MetricsDefinitionManagerEvents {
        int countFailures = 0;
        int countSuccess = 0;

        @Override
        public void onMetricsDefinitionAvailable(String application, MetricsDefinition metricsDefinition) {
            countSuccess++;
        }

        @Override
        public void onMetricsDefinitionRetrieveFailure(String application) {
            countFailures++;
        }

        public int getCountFailures() {
            return countFailures;
        }

        public int getCountSuccess() {
            return countSuccess;
        }
    }


}
