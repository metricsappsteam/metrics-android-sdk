package es.kibu.geoapis.metrics.sdk.api;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.UiThreadTestRule;
import android.support.v4.content.LocalBroadcastManager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import es.kibu.geoapis.api.Tracker;
import es.kibu.geoapis.metrics.sdk.api.events.ApplicationEvent;
import es.kibu.geoapis.metrics.sdk.api.events.MetricsEvent;
import es.kibu.geoapis.metrics.sdk.api.events.MetricsEventsReceiver;
import es.kibu.geoapis.metrics.sdk.api.events.SessionEvent;
import es.kibu.geoapis.metrics.sdk.api.events.UserEvent;
import es.kibu.geoapis.notifications.TopicHandler;

import static es.kibu.geoapis.metrics.sdk.api.events.MetricsBroadcastReceiver.METRICS_MESSAGE_ACTION;

/**
 * Created by lrodriguez2002cu on 13/01/2017.
 */
public class AndroidMetricsApiMessagingTest {


    MetricsConfig config = new MetricsConfig() {
        @Override
        public String getUser() {
            return "user1";
        }

        @Override
        public String getApplication() {
            return "application1";
        }

        @Override
        public String getSession() {
            return "session1";
        }
    };

    Tracker defaultTracker;

    @Before
    public void setup() {
        Context context = InstrumentationRegistry.getContext();
        AndroidMetricsApi.setTesting(true);
        AndroidMetricsApi.init(context, config);
        defaultTracker = AndroidMetricsApi.getInstance().getDefaultTracker(true);
    }


    class BaseTestingHandler {
        final CountDownLatch signal = new CountDownLatch(1);

        public void await(long timeout) {
            try {
                signal.await(timeout, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    class TestingApplicationHandler extends BaseTestingHandler
            implements MetricsEventsReceiver.ApplicationMetricsEventsReceiver {

        @Override
        public void onMetricsApplicationEvent(ApplicationEvent event) {
            signal.countDown();
        }

    }

    class TestingSessionHandler extends BaseTestingHandler
            implements MetricsEventsReceiver.SessionMetricsEventsReceiver {
        @Override
        public void onMetricsSessionEvent(SessionEvent event) {
            signal.countDown();
        }
    }

    class TestingUserHandler extends BaseTestingHandler
            implements MetricsEventsReceiver.UserMetricsEventsReceiver {

        @Override
        public void onMetricsUserEvent(UserEvent event) {
            signal.countDown();
        }
    }

    public static final String SampleMessage = "SampleMessage";

    String getSampleMessage(TopicHandler.TOPIC_TYPE topic){
        return String.format("SampleMessage for: %s", topic.name());
    }
    @Rule
    public UiThreadTestRule threadTestRule = new UiThreadTestRule();

    @Test
    public void registerMessagesReceiver() throws Throwable {

        final TestingApplicationHandler testingApplicationHandler = new TestingApplicationHandler();
        final TestingSessionHandler testingSessionHandler = new TestingSessionHandler();
        final TestingUserHandler testingUserHandler = new TestingUserHandler();

        threadTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //this runs on the UI thread
                AndroidMetricsApi.registerReceiver(testingApplicationHandler);

                AndroidMetricsApi.registerReceiver(testingSessionHandler);

                AndroidMetricsApi.registerReceiver(testingUserHandler);

            }
        });


        try {
            String application = "application";
            String session = "session";
            String user = "user";

            TopicHandler.TOPIC_TYPE appTopic = TopicHandler.TOPIC_TYPE.APPLICATION;
            appTopic.setApplication(application);

            TopicHandler.TOPIC_TYPE sessionTopic = TopicHandler.TOPIC_TYPE.SESSION;
            sessionTopic.setApplication(application);
            sessionTopic.setSession(session);

            TopicHandler.TOPIC_TYPE userTopic = TopicHandler.TOPIC_TYPE.USER;
            userTopic.setApplication(application);
            userTopic.setSession(session);
            userTopic.setUser(user);

            sendAndWait(testingApplicationHandler, appTopic);
            sendAndWait(testingSessionHandler, sessionTopic);
            sendAndWait(testingUserHandler, userTopic);

        } finally {

            threadTestRule.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //this runs on the UI thread
                    AndroidMetricsApi.unregisterReceiver(testingApplicationHandler);
                    AndroidMetricsApi.unregisterReceiver(testingSessionHandler);
                    AndroidMetricsApi.unregisterReceiver(testingUserHandler);

                }
            });

        }
    }

    private void sendAndWait(BaseTestingHandler appEvtReceiver, TopicHandler.TOPIC_TYPE topic) {
        sendMessage(getSampleMessage(topic), topic.getGcmTopic());
        appEvtReceiver.await(1000);
    }


    private void sendMessage(String message, String from){
        Intent intent = new Intent();
        intent.setAction(METRICS_MESSAGE_ACTION/*"es.kibu.geoapis.metrics.sdk.METRICS_MESSAGE"*/);
        intent.putExtra(MetricsEvent.DATA, message);
        intent.putExtra(MetricsEvent.TOPIC, from);
        boolean result = LocalBroadcastManager.getInstance(InstrumentationRegistry.getContext()).sendBroadcast(intent);
        Assert.assertTrue(result);
        //InstrumentationRegistry.getContext().sendBroadcast(intent);
    }

}