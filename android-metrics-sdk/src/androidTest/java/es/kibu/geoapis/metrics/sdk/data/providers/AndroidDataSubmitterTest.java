package es.kibu.geoapis.metrics.sdk.data.providers;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.metrics.sdk.data.history.RetryData;
import es.kibu.geoapis.metrics.sdk.data.history.RetryDataSaver;
import es.kibu.geoapis.objectmodel.Dimension;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;

import static es.kibu.geoapis.metrics.sdk.api.AndroidMetricsApi.METRICS_BUS;

/**
 * Created by lrodriguez2002cu on 05/09/2017.
 */
public class AndroidDataSubmitterTest {

    public static final String databaseName = "testdb1.db";

    @BeforeClass
    public static void init() {
        Context context = InstrumentationRegistry.getTargetContext();
        Configuration configuration = new Configuration.Builder(context)
                .addModelClass(RetryData.class)
                .addModelClass(es.kibu.geoapis.metrics.sdk.api.internal.DefinitionResultRecord.class)
                .addTypeSerializer(es.kibu.geoapis.metrics.sdk.data.history.ActiveAndroidSerializers.DateTimeSerializer.class)
                .setDatabaseName(databaseName)
                .create();


        ActiveAndroid.initialize(configuration, true);
        //ActiveAndroid.getDatabase().execSQL();
    }


    @AfterClass
    public static void end() {
        Context context = InstrumentationRegistry.getTargetContext();
        context.deleteDatabase(databaseName);
    }


    static class BusMessageReceiver {

        Object syncObject;
        Object retriedObj;
        Object retriedFailureObj;

        int retriedSuccessTimes;
        int retriedTimesFailed;
        int failedTimes;


        public BusMessageReceiver(Object syncObject, Object retriedObj, Object retriedFailureObj) {
            this.syncObject = syncObject;
            this.retriedFailureObj = retriedFailureObj;
            this.retriedObj = retriedObj;
        }

        @Subscribe
        public void submissionEvent(AndroidDataSubmitter.SubmissionDataEvent event) {
            // TODO: React to the event somehow!
            switch (event.getStage()) {

                case SENT:
                    throw  new RuntimeException("Unexpected to work");
                case FAILED:
                    failedTimes ++;
                    synchronized (syncObject){
                        syncObject.notify();
                    }
                    break;
                case RETRIED:
                    retriedSuccessTimes++;
                    synchronized (retriedObj){
                        retriedObj.notify();
                    }

                    break;
                case RETRIED_FAILED:
                    retriedTimesFailed++;
                    synchronized (retriedFailureObj){
                        retriedFailureObj.notify();
                    }
                    break;
            }

        }

    }

    void Await(Object syncObject) throws InterruptedException {
        synchronized (syncObject){
            syncObject.wait();
        }
    }


    @Test
    public void submitWithProblemsWillFailButRetryWillBeTriggered() throws Exception {

        Bus eventBus = new Bus(ThreadEnforcer.ANY, METRICS_BUS);

        Map<RetryData, Object> retryDataObjectMap = RetryDataSaver.loadAllUnSynczed();
        int sizeBefore = retryDataObjectMap.size();
        Log.i("SUBMITT","The size before: " +  sizeBefore);

        final Object syncObject = new Object();
        final Object retriedObject = new Object();
        final Object retriedFailureObject = new Object();
        BusMessageReceiver receiver = new BusMessageReceiver(syncObject, retriedObject, retriedFailureObject);

        Context context = InstrumentationRegistry.getTargetContext();
        eventBus.register(receiver);

        //this must fail as the url is wrong.
        AndroidDataSubmitter submitter = new AndroidDataSubmitter("wrong_url", context, new Configurable() {
            @Override
            public String getUser() {
                return "user1";
            }

            @Override
            public String getApplication() {
                return "application1";
            }

            @Override
            public String getSession() {
                return "session1";
            }
        }, eventBus);


        submitter.submit("hello", new DataContext() {
            @Override
            public MetricsDefinition getDefinition() {
                return null;
            }

            @Override
            public Variable getVariable() {
                Variable variable = new Variable();
                variable.setName("variable1");
                return variable;
            }

            @Override
            public Dimension getDimension() {
                return new Dimension() {
                    @Override
                    public String getName() {
                        return "dim1";
                    }
                };
            }
        });

        Await(syncObject);
        //if wait for the object will never be able to exit from it as si never signaled
        //Await(retriedObject);
        Await(retriedFailureObject);

        retryDataObjectMap = RetryDataSaver.loadAllUnSynczed();
        int size = retryDataObjectMap.size();

        Log.i("SUBMITT","The size after: " +  size);
        //Assert.assertTrue((size - sizeBefore) == 1);
        Assert.assertTrue(receiver.failedTimes == 1);

        Assert.assertTrue(receiver.retriedSuccessTimes == 0);
        Assert.assertTrue(receiver.retriedTimesFailed <= size);


    }

}