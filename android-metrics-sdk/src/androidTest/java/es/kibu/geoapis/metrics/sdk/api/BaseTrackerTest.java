package es.kibu.geoapis.metrics.sdk.api;

import org.junit.Ignore;
import org.junit.Test;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import es.kibu.geoapis.api.BaseTracker;
import es.kibu.geoapis.api.CoreMetricsApiException;
import es.kibu.geoapis.api.Tracker;
import es.kibu.geoapis.data.providers.IntegerProvider;
import es.kibu.geoapis.objectmodel.CustomDimension;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.BasicMetricsDefinitionReader;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by lrodriguez2002cu on 06/09/2016.
 */
public abstract class BaseTrackerTest {


    public static MetricsDefinition withMetrics(String metricsDoc) {
        ClassLoader classLoader = BaseTrackerTest.class.getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream(metricsDoc);
        BasicMetricsDefinitionReader metricsDefinitionReader = new BasicMetricsDefinitionReader();
        return metricsDefinitionReader.readFrom(resourceAsStream);
    }

/*
    protected Tracker createTracker() {
        TrackerFactory factory = DaggerTrackerFactory.create();
        //DaggerTrackerFactory.builder().providersModule().
        Tracker tracker = factory.createTracker();
        return tracker;
    }
*/
    protected abstract Tracker createTracker();

    @Test
    public void testProviderLoadingShouldSuccess() {
        Tracker dataCollector = createTracker();
        ((BaseTracker) dataCollector).loadProviderClass(IntegerProvider.class.getName(), "sample");
        assertTrue(((BaseTracker) dataCollector).getRegistry().getProvider("sample") != null);
    }

    @Test
    public void testProviderLoadingUnknownClassShouldFail() {
        try {
            Tracker dataCollector = createTracker();
            //the class does  not exists
            ((BaseTracker) dataCollector).loadProviderClass("classnametofail", "sample");
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof CoreMetricsApiException);
        }

        try {
            Tracker dataCollector = createTracker();
            ((BaseTracker) dataCollector).loadProviderClass(Integer.class.getName(), "sample");
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof CoreMetricsApiException);
        }
    }

    @Test
    public void testLoadingAClassWithProviders() {

        MetricsDefinition metricsDefinition = withMetrics("sample-documents/sample-metric_classprovider.json");
        Tracker dataCollector = createTracker();
        dataCollector.setMetricsDefinition(metricsDefinition);
        assertTrue(((BaseTracker) dataCollector).getRegistry().getProvider("temperatureDimension") != null);

    }

    @Test
    public void testCoreApiWithCompleteVariable() {

        MetricsDefinition metricsDefinition = withMetrics("sample-documents/sample-metric_classprovider.json");
        Tracker dataCollector = createTracker();
        dataCollector.setMetricsDefinition(metricsDefinition);
        dataCollector.send("textLength", "size", 20);

    }

    @Test
    public void testCoreApiWithIncompleteVariableMustFail() {

        String metricsDoc = "sample-documents/sample-metric_classprovider.json";
        MetricsDefinition metricsDefinition = withMetrics(metricsDoc);

        try {
            Tracker dataCollector = createTracker();
            dataCollector.setMetricsDefinition(metricsDefinition);
            dataCollector.send("shots", "shotsStrength", 20);
            fail();
        } catch (CoreMetricsApiException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCoreApiWithIncompleteVariableMustFail1() {
        String metricsDoc = "sample-documents/sample-metric_classprovider.json";
        MetricsDefinition metricsDefinition = withMetrics(metricsDoc);
        try {
            Tracker dataCollector = createTracker();
            dataCollector.setMetricsDefinition(metricsDefinition);
            Map<String, Object> dimsValues = new HashMap<>();
            dimsValues.put("shotsStrength", 20);
            dataCollector.send("shots", dimsValues);
            fail();
        } catch (CoreMetricsApiException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCoreApiWithIncompleteVariableMustFail2() {
        String metricsDoc = "sample-documents/sample-metric_classprovider.json";
        MetricsDefinition metricsDefinition = withMetrics(metricsDoc);
        try {
            Tracker dataCollector = createTracker();
            dataCollector.setMetricsDefinition(metricsDefinition);
            Map<String, Object> dimsValues = new HashMap<>();
            //note that the dimValues is empty
            dataCollector.send("shots", dimsValues);
            fail();
        } catch (CoreMetricsApiException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void testCoreApiWithCompleteVariablesMustBeOk() {
        String metricsDoc = "sample-documents/sample-metric_classprovider.json";
        MetricsDefinition metricsDefinition = withMetrics(metricsDoc);
        Tracker tracker = createTracker();
        tracker.setMetricsDefinition(metricsDefinition);

        Map<String, Object> dimsValues = new HashMap<>();
        dimsValues.put("shotsStrength", 20);
        dimsValues.put("shotsReach", 10);

        tracker.send("shots", dimsValues);

    }


    @Test
    public void testCoreApiWithCompleteVariable1() {

        MetricsDefinition metricsDefinition = withMetrics("sample-documents/sample-metric_classprovider.json");
        Tracker dataCollector = createTracker();
        dataCollector.setMetricsDefinition(metricsDefinition);
        Map<String, Object> dimValues = new HashMap<>();
        dimValues.put("size", 20);
        dataCollector.send("textLength", dimValues);

    }

    @Test
    @Ignore
    public void testCoreApiWithCompleteCustomObject() {

        MetricsDefinition metricsDefinition = withMetrics("sample-documents/sample_metrics_custom_object.json");
        Tracker dataCollector = createTracker();
        dataCollector.setMetricsDefinition(metricsDefinition);

        assertTrue(metricsDefinition.getVariableByName("textLength").getDimension("objDimension").isCustomDimension());
        assertTrue(!((CustomDimension) metricsDefinition.getVariableByName("textLength").getDimension("objDimension")).getDimensionSchema().isEmpty());

        //all dimensions are missing
        try {
            Map<String, Object> dimValues = new HashMap<>();
            dimValues.put("size", 20);

            Map<String, Object> objDim = new HashMap<>();
            dimValues.put("objDimension", objDim);
            dataCollector.send("textLength", dimValues);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //it is ok, everything  is working fine
        try {
            Map<String, Object> dimValues = new HashMap<>();
            dimValues.put("size", 20);

            Map<String, Object> objDim = new HashMap<>();
            dimValues.put("objDimension", objDim);

            objDim.put("firstName", "name");
            objDim.put("lastName", "lastname");
            objDim.put("age", 10);

            dataCollector.send("textLength", dimValues);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

        //wrong type
        try {
            Map<String, Object> dimValues = new HashMap<>();
            dimValues.put("size", 20);

            Map<String, Object> objDim = new HashMap<>();
            dimValues.put("objDimension", objDim);

            objDim.put("firstName", "name");
            objDim.put("lastName", 1);
            objDim.put("age", 10);

            dataCollector.send("textLength", dimValues);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //missing required property
        try {
            Map<String, Object> dimValues = new HashMap<>();
            dimValues.put("size", 20);

            Map<String, Object> objDim = new HashMap<>();
            dimValues.put("objDimension", objDim);

            objDim.put("firstName", "name");
            objDim.put("age", 10);

            dataCollector.send("textLength", dimValues);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testCoreApiWithUnknownVariableMustFail() {

        MetricsDefinition metricsDefinition = withMetrics("sample-documents/sample-metric_classprovider.json");
        Tracker dataCollector = createTracker();
        dataCollector.setMetricsDefinition(metricsDefinition);
        Map<String, Object> dimValues = new HashMap<>();
        String dimName = "unknownDimension";
        dimValues.put(dimName, 20);
        try {
            //using the map api
            dataCollector.send("textLength", dimValues);
        } catch (CoreMetricsApiException e) {
            e.printStackTrace();
        }

        try {
            //using the second api
            dataCollector.send("textLength", dimName, 20);
        } catch (CoreMetricsApiException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCoreApiWithUnknownDimensionMustFail() {

        MetricsDefinition metricsDefinition = withMetrics("sample-documents/sample-metric_classprovider.json");
        Tracker dataCollector = createTracker();
        dataCollector.setMetricsDefinition(metricsDefinition);
        Map<String, Object> dimValues = new HashMap<>();
        String dimName = "size";
        dimValues.put(dimName, 20);
        try {
            //using the map api
            dataCollector.send("unknownVariable", dimValues);
        } catch (CoreMetricsApiException e) {
            e.printStackTrace();
        }

        try {
            //using the second api
            dataCollector.send("unknownVariable", dimName, 20);
        } catch (CoreMetricsApiException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCoreApiWithCompleteNonCustomDimensionVariable() {

        MetricsDefinition metricsDefinition = withMetrics("sample-documents/sample_metrics.json");
        Tracker dataCollector = createTracker();
        dataCollector.setMetricsDefinition(metricsDefinition);

        dataCollector.send("justLocation");

        try {
            dataCollector.send("justLocation", "aa", "mm");
            fail();
        }
        catch(CoreMetricsApiException ae) {

        }

    }

}