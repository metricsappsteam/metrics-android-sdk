package es.kibu.geoapis.metrics.sdk.data.services;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.joda.time.DateTime;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import es.kibu.geoapis.metrics.MetricsCoreUtils;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.BasicMetricsDefinitionReader;
import es.kibu.geoapis.serialization.BasicMetricsDefinitionWriter;
import es.kibu.geoapis.services.objectmodel.results.CreateResult;
import es.kibu.geoapis.services.objectmodel.results.DeleteResult;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;
import es.kibu.geoapis.services.objectmodel.results.ResultValue;
import es.kibu.geoapis.services.objectmodel.results.ResultValueReader;
import es.kibu.geoapis.services.objectmodel.results.UpdateResult;

//import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by lrodr_000 on 04/01/2017.
 */
public abstract class BaseServicesTest {

    public static final String BASE_CREATE = "/api/v1/create-metrics/";
    public static final String BASE_UPDATE = "/api/v1/update-metrics/";
    public static final String BASE_DELETE = "/api/v1/delete-metrics/";
    public static final String BASE_SELECT = "/api/v1/metrics/";
    public static final String BASE_EXIST = "/api/v1/metrics-exist/";
    public static final String SAMPLE_APPID = "sample-appid";
    public static final String RESOURCES_SAMPLE_METRIC_WIKILOC_BOX_SPEED_GEOSTATS_JSON = "resources/sample_metric.json";

    Logger logger = Logger.getLogger(BaseServicesTest.class.getName());

    protected BasicMetricsDefinitionReader getReader() {
        return new BasicMetricsDefinitionReader();
    }

    protected BasicMetricsDefinitionWriter getWriter() {
        return new BasicMetricsDefinitionWriter();
    }


    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        //assertThat(a).isEqualTo(2);
    }

    private void debug(String message) {
        Log.d(this.getClass().getCanonicalName(), message);
    }

    public static class Json {
        public static JsonNode parse(String json) throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readTree(json);
        }

        public static String toJson(Object object) throws JsonProcessingException {
            ObjectMapper mapper = new ObjectMapper();
            //Object to JSON in String
            String jsonInString = mapper.writeValueAsString(object);
            return jsonInString;
        }
    }

    protected abstract InputStream getMetricsDefinitionStream(String filename);


    @Test
    public void testPreconditions() throws Exception {

        MetricsDefinition definition = getMetricsDefinition(RESOURCES_SAMPLE_METRIC_WIKILOC_BOX_SPEED_GEOSTATS_JSON);
        assertTrue(definition.getVariables().size()>0)/*.isGreaterThan(0)*/;

        BasicMetricsDefinitionWriter writer = getWriter();
        JsonNode jsonNode = Json.parse(writer.write(definition));

        String converted = jsonNode.toString();

        debug(converted);
        BasicMetricsDefinitionReader reader = new BasicMetricsDefinitionReader();
        MetricsDefinition definition1 = reader.readFrom(converted);

        assertEquals(definition.getVariables(), definition1.getVariables());

    }

    private MetricsDefinition getMetricsDefinition(String resourcesSampleMetricWikilocBoxSpeedGeostatsJson) throws Exception {
        InputStream in = getMetricsDefinitionStream(resourcesSampleMetricWikilocBoxSpeedGeostatsJson);
        return MetricsCoreUtils.withMetricDefinition(in);
    }

    @Test
    public void testCreateMetric() throws Exception {
        String applicationId = getRandomAppId();
        checkCreateMetric(applicationId, 200);
    }

    @Test
    public void creatingAMetricForTheSameAppTwiceShouldFail() throws Exception {
        String applicationId = getRandomAppId();
        checkCreateMetric(applicationId, 200);
        checkCreateMetric(applicationId, 400);
    }

    protected String getRandomAppId() {
        return String.format("sample-appid%s", Long.toString(DateTime.now().getMillis()));
    }

   /* @Test
    public void gettingAMetricForANonExistingAppShouldFail() throws IOException, Exception {
        String applicationId = getRandomAppId();
        boolean isEmpty = checkGetMetric(applicationId, 200);
        assertTrue(isEmpty);
    }*/

    @Test
    public void updatingAMetricForNonExistignAppShouldFail() throws Exception {
        String applicationId = getRandomAppId();
        checkUpdateForApp(applicationId, 400);
    }

    @Test
    public void testDelete() throws Exception {
        String applicationId = getRandomAppId();
        checkDeleteMetric(applicationId, 200, "delete_failed");
    }

    @Test
    public void testDeleteComplex() throws Exception {
        String applicationId = getRandomAppId();
        checkCreateMetric(applicationId, 200);
        checkDeleteMetric(applicationId, 200, "deleted");
    }

    @Test
    public void testUpdateMetric() throws Exception {

        String applicationId = getRandomAppId();
        try {
            Log.d("CREATING_METRIC", "Creating metric for app: " + applicationId);
            checkCreateMetric(applicationId, 200);

            checkUpdateForApp(applicationId, 200);

        }finally {
            checkDeleteMetric(applicationId, 200, "deleted");
        }


    }

    @Test
    public void gettingAMetricForANonExistingAppShouldFail() throws IOException{
        String applicationId = getRandomAppId();
        boolean isEmpty = checkGetMetric(applicationId, 400);
        assertTrue(isEmpty);
    }

    @Test
    public void testMetricExists() throws Exception {

        String applicationId = "sample-appid";

        checkMetricsExist(applicationId);
    }

    @Test
    public void testGetMetric() throws Exception{
        //String applicationId = "sample-appid";

        String applicationId = getRandomAppId();
        try {
            //Log.d("CREATING_METRIC", "Creating metric for app: " + applicationId);
            checkCreateMetric(applicationId, 200);
            checkGetMetric(applicationId, 200);

        }finally {
            checkDeleteMetric(applicationId, 200, "deleted");
        }
    }


    private void checkMetricsExist(String applicationId) throws Exception{

        String content = getMetricsExistResult(applicationId, 200);
        ResultValue<ExistResult> existResult = ResultValueReader.asExistResult(content);
        assertTrue(existResult.getResult().isResult());
        assertTrue(existResult.getResult().getHash() != null);
        assertTrue(!existResult.getResult().getHash().isEmpty());

    }

   /* private boolean checkGetMetric(String applicationId) throws IOException {

        String content = getGetMetricResult(applicationId, 200);

        JsonNode parse = Json.parse(content);
        assertTrue(parse.has("result"));

        assertTrue(parse.get("result").asText().equalsIgnoreCase("empty") || parse.get("result").has("content"));

        if (!parse.get("result").isTextual() && parse.get("result").has("content")) {
            assertTrue(parse.get("result").has("hash"));
            assertTrue(parse.get("result").has("isPublic"));
            assertTrue(parse.get("result").has("metricId"));
        }
        return parse.get("result").isTextual() && parse.get("result").asText().equalsIgnoreCase("empty");
    }*/

    private boolean checkGetMetric(String applicationId, int expectedCode) throws IOException {

        String content = getGetMetricResult(applicationId, expectedCode);
        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            assertTrue(parse.has("result"));
            assertTrue(/*parse.get("result").asText().equalsIgnoreCase("empty") || */ parse.get("result").has("content"));

            if (!parse.get("result").isTextual() && parse.get("result").has("content")) {
                assertTrue(parse.get("result").has("hash"));
                assertTrue(parse.get("result").has("isPublic"));
                assertTrue(parse.get("result").has("metricId"));
            }
        } else {
            return parse.get("errorMessage").isTextual() && parse.get("errorMessage").asText().contains("No metrics for app") /*parse.get("result").isTextual() && parse.get("result").asText().equalsIgnoreCase("empty")*/;
        }
        return false;
    }

    protected void checkDeleteMetric(String applicationId, int expectedCode, String resultDeleted) throws Exception {

        String content = getDeleteMetricResult(applicationId, expectedCode);

        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            ResultValue<DeleteResult> deleteResultResultValue = ResultValueReader.asDeleteResult(content);
            assertTrue(deleteResultResultValue.getResult().getResult().equalsIgnoreCase(resultDeleted));

        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
        }
    }

    protected void checkUpdateForApp(String applicationId, int expectedCode) throws Exception{
        MetricsDefinition definition = getMetricsDefinition("sample_metric.json");
        BasicMetricsDefinitionWriter writer = getWriter();
        JsonNode jsonNode = Json.parse(writer.write(definition));

        String content = getUpdateResult(applicationId, jsonNode, expectedCode);

        JsonNode parse = Json.parse(content);

        if (expectedCode == 200) {
            ResultValue<UpdateResult> deleteResultResultValue = ResultValueReader.asUpdateResult(content);
            assertTrue(deleteResultResultValue.getResult().getResult().equalsIgnoreCase("updated"));
        } else {
            assertTrue(parse.get("error").asBoolean());
        }
    }



    protected void checkCreateMetric(String applicationId, int expectedCode) throws Exception {
        MetricsDefinition definition = getMetricsDefinition("sample_metric.json");
        BasicMetricsDefinitionWriter writer = getWriter();
        JsonNode jsonNode = Json.parse(writer.write(definition));

        String content = getCreateResult(applicationId, jsonNode, expectedCode);

        JsonNode parse = Json.parse(content);
        if (expectedCode == 200) {
            ResultValue<CreateResult> resultValue = ResultValueReader.asCreateResult(content);
            assertTrue(resultValue.getResult().getResult().equalsIgnoreCase("created"));
            assertTrue(resultValue.getResult().getId() != null);
            assertTrue(!resultValue.getResult().getId().isEmpty());

        } else {
            assertTrue(parse.has("error") && parse.get("error").asBoolean());
        }

    }

    protected abstract String getCreateResult(String applicationId, JsonNode jsonNode, int expectedCode);

    protected abstract String getUpdateResult(String applicationId, JsonNode jsonNode, int expectedCode);

    protected abstract String getMetricsExistResult(String applicationId, int expectedCode);

    protected abstract String getGetMetricResult(String applicationId, int expectedCode);

    protected abstract String getDeleteMetricResult(String applicationId, int expectedCode);
}
