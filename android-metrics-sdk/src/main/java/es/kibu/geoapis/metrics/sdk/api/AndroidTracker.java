package es.kibu.geoapis.metrics.sdk.api;


import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import es.kibu.geoapis.api.BaseTracker;
import es.kibu.geoapis.api.DataSubmitter;
import es.kibu.geoapis.api.WiredTracker;
import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.objectmodel.Variable;

/**
 * Created by lrodriguez2002cu on 08/11/2016.
 */

public class AndroidTracker extends WiredTracker/*BaseTracker */{

    /*@Inject
    DataSubmitter submitter;
*/
    @Inject
    public AndroidTracker() {
    }

    /*Variable variable;

    Map<String, Object> resultObject;

    private Object getResultObject() {
        return  resultObject;
    }

    @Override
    public void send(String variableName, String dimensionName, Object data) {
        newResultObject();
        super.send(variableName, dimensionName, data);
        DataContext dataContext = getDataContext(variableName, dimensionName);
        submitter.submit(getResultObject(),  dataContext);
    }

    @Override
    public void send(String variableName, Map<String, Object> eventData) {
        newResultObject();
        super.send(variableName, eventData);
        DataContext dataContext = getDataContext(variableName, "");
        submitter.submit(getResultObject(), dataContext);
    }

    private void newResultObject() {
        resultObject = new HashMap<>();
    }

    @Override
    protected void add(Variable variableByName, String dimensionName, Object data) {
        variable = variableByName;
        resultObject.put(dimensionName, data);
    }*/
}
