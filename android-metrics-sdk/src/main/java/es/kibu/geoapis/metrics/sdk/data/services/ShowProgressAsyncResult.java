package es.kibu.geoapis.metrics.sdk.data.services;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Class for encapsulating the creation of an async result having at the same
 * time the functionality for showing a ProgressBar during the invocation of the
 * operation related to the AsyncResult.
 * 
 * @author lrodriguez
 * 
 * @param <Result>
 *            the class of the expected result as it would be pass to
 *            AsyncResult.
 */
public abstract class ShowProgressAsyncResult<Result> extends
		AsyncResult<Result> {

	Activity activity;

	static class ShowDialogProgressHandler implements ProgressHandler {

		Activity activity;
		ProgressDialog dialog;

		public ProgressDialog getDialog() {
			return dialog;
		}

		public ShowDialogProgressHandler(Activity activity,
				String initialMessage) {
			this.activity = activity;
			if (dialog == null) {
				dialog = ProgressDialog
						.show(activity, "", initialMessage, true);
				dialog.show();
			}
		}

		public ShowDialogProgressHandler(Activity activity) {
			this(activity, "Starting operation");
		}

		@Override
		public void notifyProgress(String message, Object associatedObject,
								   int progress) {
			dialog.setMessage(message);
		}
	}

	public ShowProgressAsyncResult(Activity activity) {
		super(new ShowDialogProgressHandler(activity));
	}

	public ShowProgressAsyncResult(Activity activity, String initialMessage) {
		super(new ShowDialogProgressHandler(activity, initialMessage));
	}

	private ProgressDialog getDialog() {
		return ((ShowDialogProgressHandler) this.getProgressHandler())
				.getDialog();
	}

	@Override
	public final void onFailure(Exception ex) {
		closeDialog();
		onOperationFailure(ex);
	}

	public abstract void onOperationFailure(Exception ex);

	@Override
	public final void onSuccess(Result result) {
		closeDialog();
		onOperationSuccess(result);
	}

	public abstract void onOperationSuccess(Result result);

	public void closeDialog() {
		if (getDialog().isShowing())
			getDialog().cancel();
	}

	/**
	 * 
	 */

	@SuppressWarnings("unused")
	private void sampleUsage() {
		// AsyncServiceClient sc = application.getServiceClient();
		//
		// AsyncResult<List<GameDescription>> asyncResult = new
		// ShowProgressAsyncResult<List<GameDescription>>(this,
		// getString(R.string.retrieving_your_games_)){
		//
		// @Override
		// public void onOperationFailure(Exception ex) {
		// AppUtils.showToast("Error while calling a service with:" +
		// ex.getMessage(), GetMyGamesActivity.this);
		// }
		//
		// @Override
		// public void onOperationSuccess(List<GameDescription> result) {
		// List<Item> allItems = itemsFromGameDescriptions(result);
		// GetMyGamesActivity.this.setupList(allItems);
		// }
		//
		// };
		//
		// try {
		// sc.listOngoingGamesAsync(asyncResult);
		// } catch (SerializationException e) {
		// application.handleError(e, this);
		// }
	}
}
