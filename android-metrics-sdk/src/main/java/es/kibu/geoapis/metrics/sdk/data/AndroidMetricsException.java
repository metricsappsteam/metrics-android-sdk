package es.kibu.geoapis.metrics.sdk.data;

import es.kibu.geoapis.api.CoreMetricsApiException;

/**
 * Created by lrodriguez2002cu on 05/09/2017.
 */

public class AndroidMetricsException extends CoreMetricsApiException {

    public AndroidMetricsException(String message) {
        super(message);
    }
}
