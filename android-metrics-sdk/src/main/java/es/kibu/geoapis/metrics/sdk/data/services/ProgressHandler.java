package es.kibu.geoapis.metrics.sdk.data.services;

public interface ProgressHandler {
	class Utils {
		public static void notifyProgress(ProgressHandler handler,
										  String message, Object associatedObject, int progress) {
			if (handler != null) {
				// Ignore the possible exceptions in the handlers.
				try {
					handler.notifyProgress(message, associatedObject, progress);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	void notifyProgress(String message, Object associatedObject,
                        int progress);

}