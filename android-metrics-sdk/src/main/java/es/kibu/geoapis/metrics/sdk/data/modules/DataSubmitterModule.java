package es.kibu.geoapis.metrics.sdk.data.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.otto.Bus;

import dagger.Module;
import dagger.Provides;
import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.api.DataSubmitter;
import es.kibu.geoapis.metrics.sdk.api.MetricsConfig;
import es.kibu.geoapis.metrics.sdk.configuration.ConfigurationManager;
import es.kibu.geoapis.metrics.sdk.configuration.ServiceConfiguration;
import es.kibu.geoapis.metrics.sdk.data.providers.AndroidDataSubmitter;
import es.kibu.geoapis.metrics.sdk.data.services.MetricsAsyncServiceClient;


/**
 * Created by lrodriguez2002cu on 08/11/2016.
 */

@Module
public class DataSubmitterModule {

    public static final String TAG = "Anonymous submitter";
    private MetricsAsyncServiceClient client;

    Context context;
    ConfigurationManager configurationManager;
    ServiceConfiguration serviceConfiguration;
    Configurable metricsConfig;
    boolean forTesting;
    Bus bus;

    public String getBaseUrl() {
        return serviceConfiguration.getBaseURL();
    }

    public DataSubmitterModule(Context context, Configurable metricsConfig, Bus bus, boolean forTesting) {
        this(null, context, metricsConfig, bus, forTesting);
    }

    public DataSubmitterModule(MetricsAsyncServiceClient client, Context context, Configurable metricsConfig, Bus bus, boolean forTesting) {
        if (context == null)
            throw new RuntimeException("Context must not be null for creating the DataSubmitter");
        this.context = context;
        this.configurationManager = ConfigurationManager.getInstance(context);
        this.serviceConfiguration = configurationManager.getServicesConfiguration();
        this.metricsConfig = metricsConfig;
        this.forTesting = forTesting;
        this.client = client;
        this.bus = bus;
    }

    @Provides
    DataSubmitter provideSubmitter() {
        return getAndroidSubmitter();
    }

    @NonNull
    private DataSubmitter getAndroidSubmitter() {
        return (forTesting) ? getDummyDataSubmitter() : getAndroidDataSubmitter();
    }

    @NonNull
    private AndroidDataSubmitter getAndroidDataSubmitter() {
        if (client != null) {
            return new AndroidDataSubmitter(client, context, metricsConfig, bus);
        } else
            return new AndroidDataSubmitter(getBaseUrl(), context, metricsConfig, bus);
    }

    @NonNull
    private DataSubmitter getDummyDataSubmitter() {
        return new AndroidDataSubmitter.DummySubmitter();
    }
}
