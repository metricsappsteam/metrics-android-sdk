package es.kibu.geoapis.metrics.sdk.data.history;

import com.activeandroid.serializer.TypeSerializer;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.activeandroid.serializer.TypeSerializer;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Created by lrodriguez2002cu on 03/01/2017.
 */

public class ActiveAndroidSerializers {

    public static class DateTimeSerializer extends TypeSerializer {

        DateTimeFormatter fmt = ISODateTimeFormat.dateTime();

        @Override
        public Class<?> getDeserializedType() {
            return DateTime.class;
        }

        @Override
        public Class<?> getSerializedType() {
            return String.class;
        }

        @Override
        public Object serialize(Object o) {
            if (o instanceof DateTime) {
                String result = fmt.print((DateTime) o);
                return result;
            }
            throw new RuntimeException("Invalid object class for this serializer");
        }

        @Override
        public Object deserialize(Object o) {
            DateTime result = fmt.parseDateTime((String) o);
            return result;
        }
    }

}
