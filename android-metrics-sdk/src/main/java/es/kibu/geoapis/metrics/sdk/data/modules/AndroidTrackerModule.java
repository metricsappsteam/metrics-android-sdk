package es.kibu.geoapis.metrics.sdk.data.modules;

import dagger.Module;
import dagger.Provides;
import es.kibu.geoapis.api.BaseTracker;
import es.kibu.geoapis.data.providers.modules.TrackerModule;
import es.kibu.geoapis.metrics.sdk.api.AndroidTracker;

/**
 * Created by lrodriguez2002cu on 08/11/2016.
 */

@Module
public class AndroidTrackerModule extends TrackerModule {
    @Provides
    public BaseTracker getTracker(AndroidTracker tracker){
        return tracker;
    }
}
