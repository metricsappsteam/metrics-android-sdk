package es.kibu.geoapis.metrics.sdk.configuration;

import android.content.Context;

import es.kibu.geoapis.metrics.sdk.data.services.DefaultResponseDeserializer;

/**
 * Created by lrodriguez2002cu on 15/05/2016.
 */
public class ConfigurationManager {


    static ConfigurationManager  _instance;

    //AuthConfiguration authConfiguration;
    Configuration configuration;

    protected ConfigurationManager(Context context) {
        ConfigurationReader reader = ConfigurationReaderFactory.getReader(context);
        configuration = reader.readConfiguration();
    }

    public static ConfigurationManager getInstance(Context context) {
        if (_instance == null){
            _instance = new ConfigurationManager(context);
        }
        return _instance;
    }

    public static class ConfigurationReaderFactory{
        public static ConfigurationReader getReader(Context context) {
            return new ManifestConfigurationReader(context);
        }
    }


    public Configuration getConfiguration(){
        return configuration;
    }

    public ServiceConfiguration getServicesConfiguration(){
        return new ServiceConfiguration(getConfiguration());
    }

   /* public AuthConfiguration getConfiguration(){
        AuthConfiguration authConfiguration = AuthDataCreator.getAuthConfiguration(configuration);
        return authConfiguration;
    }*/
}
