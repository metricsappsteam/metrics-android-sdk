package es.kibu.geoapis.metrics.sdk.data.services;

import android.app.Activity;
import android.view.View;
import android.widget.ProgressBar;

public abstract class UseProgressAsyncResult<Result> extends
		AsyncResult<Result> {

	public UseProgressAsyncResult(Activity activity, ProgressBar bar) {
		super(new ViewProgressHandler(activity, bar));
	}

	static class ViewProgressHandler implements ProgressHandler {

		Activity activity;
		ProgressBar progressBar;

		public ProgressBar getProgressBar() {
			return progressBar;
		}

		public ViewProgressHandler(Activity activity, ProgressBar bar,
								   String initialMessage) {
			this.activity = activity;
			progressBar = bar;
			if (progressBar != null) {
				progressBar.setVisibility(View.VISIBLE);
			}
		}

		public ViewProgressHandler(Activity activity, ProgressBar bar) {
			this(activity, bar, "Starting operation");
		}

		@Override
		public void notifyProgress(String message, Object associatedObject,
								   int progress) {
			// dialog.setMessage(message);
		}
	}

	@Override
	public final void onFailure(Exception ex) {
		hideProgressBar();
		onOperationFailure(ex);
	}

	public abstract void onOperationFailure(Exception ex);

	@Override
	public final void onSuccess(Result result) {
		hideProgressBar();
		onOperationSuccess(result);
	}

	public abstract void onOperationSuccess(Result result);

	ProgressBar getProgressBar() {
		ViewProgressHandler progressHandler = (ViewProgressHandler) getProgressHandler();
		return progressHandler.getProgressBar();
	}

	public void hideProgressBar() {
		ProgressBar bar = getProgressBar();
		if (bar != null) {
			bar.setVisibility(View.GONE);
		}
	}

	/**
	 * 
	 */

}
