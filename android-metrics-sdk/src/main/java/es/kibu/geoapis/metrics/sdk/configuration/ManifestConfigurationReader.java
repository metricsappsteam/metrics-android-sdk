package es.kibu.geoapis.metrics.sdk.configuration;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

/**
 * Created by lrodriguez2002cu on 15/05/2016.
 */
public class ManifestConfigurationReader implements ConfigurationReader {

    public static final String TAG = "MConfigReader";
    public static final String METRICS_BASE_KEY = "metrics-";
    private final Context context;

    public ManifestConfigurationReader(Context context) {
        this.context = context;
    }

    @Override
    public Configuration readConfiguration() {

        Configuration configuration = new Configuration();
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            for (String key : applicationInfo.metaData.keySet()) {
               if (key.toLowerCase().contains(METRICS_BASE_KEY)) {
                   Object value = applicationInfo.metaData.get(key);
                   if (value instanceof String){
                      configuration.add(key, (String) value);
                   }
               }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return configuration;
    }
}
