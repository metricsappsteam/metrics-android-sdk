package es.kibu.geoapis.metrics.sdk.data.services;

import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.services.objectmodel.results.CreateResult;
import es.kibu.geoapis.services.objectmodel.results.DeleteResult;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;
import es.kibu.geoapis.services.objectmodel.results.SubmitResult;
import es.kibu.geoapis.services.objectmodel.results.UpdateResult;

/**
 * Created by lrodriguez2002cu on 03/01/2017.
 *
 * This interface includes the basic functionality for accessing metrics-services.
 */
public interface MetricsAsyncServiceClient {

    /**
     * Submits the metrics data. This is the way of providing the metrics data to the system.
     * @param applicationId the applicationId
     * @param data the data to be submitted
     * @param result the asyncResult, an interface containing the mechamism for receiving the data in
     *               an asynchronous way.
     *               @see SubmitResult for especific details on the return type of this service.
     */
    void submitDataAsync(String applicationId, String variable, Object data, AsyncResult<SubmitResult> result);

    /**
     * Retrieves the metric definition installed for the given application. Only one can exist for
     * a given application.
     * @param applicationId the applicationId
     * @param result the asyncResult, an interface containing the mechamism for receiving the data in
     *               an asynchronous way.
     *               @see MetricsResult for especific details on the return type of this service.
     */
    void metricsAsync(String applicationId, AsyncResult<MetricsResult> result);

    /**
     * Checks if the metrics exist for a given application.
     * @param applicationId the applicationId
     * @param result the asyncResult, an interface containing the mechamism for receiving the data in
     *               an asynchronous way.
     *               @see ExistResult for especific details on the return type of this service.
     */
    void metricsExistAsync(String applicationId, AsyncResult<ExistResult> result);


    /**
     * Creates the metric definition for a given application.
     * @param applicationId the applicationId
     * @param definition the metrics Definition to be installed for the application
     * @param result the asyncResult, an interface containing the mechamism for receiving the data in
     *               an asynchronous way.
     *               @see CreateResult for especific details on the return type of this service.
     */
    void createMetricAsync(String applicationId, MetricsDefinition definition, AsyncResult<CreateResult> result);


    /**
     * Deletes the metrics definition for a given app.
     * @param applicationId the applicationId
     * @param result the asyncResult, an interface containing the mechanism for receiving the data in
     *               an asynchronous way.
     *               @see DeleteResult for especific details on the return type of this service.
     */
    void deleteMetricsAsync(String applicationId, AsyncResult<DeleteResult> result);

    /**
     * Updates the metrics installed for a given application
     * @param applicationId the applicationId
     * @param definition  definition the metrics Definition to be installed for the application
     * @param result the asyncResult, an interface containing the mechanism for receiving the data in
     *               an asynchronous way.
     *               @see UpdateResult for especific details on the return type of this service.
     */
    void updateMetricsAsync(String applicationId, MetricsDefinition definition, AsyncResult<UpdateResult> result);

}
