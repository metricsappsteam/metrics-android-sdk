package es.kibu.geoapis.metrics.sdk.data.services;


/**
 * This class is used to hold the result of invoking asynchronously a web service using the asynchronous version of 
 * the web services.
 * The objects of this class can hold successful or failure responses. In the case of successful responses the property isFailed()
 * is going to be set to true and false otherwise. Besides there is the possibility of performing actions when the operations are 
 * finished. For that, is necessary to implement the onFailure and onSuccess methods that are going to be called after completion 
 * of the asynchronous operation. 
 * @author lrodriguez
 *
 * @param <Result> The class of the result of the asynchronous operation to be called.
 */

public abstract class AsyncResult<Result> {
	private Result result;
	private boolean callFailed;
	private Exception failureException;
	private ProgressHandler progressHandler;

	/**
	 * Method invoked by the asynchronous versions of the clients when a failure occurs 
	 * when invoking a  web service method. Override this method with the code to handling a failure in the invocation.
	 * @param ex The instance of the exception occurred. 
	 */
	public abstract void onFailure(Exception ex);
	
	/**
	 * Method invoked by the asynchronous versions of the clients when the invocation of a web method is successful  
	 * Override this method with the code to handling the successful completion of the method invoked.
	 * @param result the result of invoking the method.
	 */
	public abstract void onSuccess(Result result);
	
	/**
	 * Constructor accepting a progress handler, used for receiving notifications during the web method invocation.
	 * @param handler the progress handler.
	 */
	public AsyncResult(ProgressHandler handler){
		this.progressHandler = handler;
	}
	
	/**
	 * Allows to put the asyncresult object in the 'success' state. 
	 * @param result the result of the invocation of the service.
	 */
	public void setSuccessResult(Result result){
		this.setResult(result);
		this.callFailed= false;
		this.setFailureException(null);
	}

	/**
	 * Alows to put the asyncresult object in the 'failure' state.
	 * @param e Exception occurred  provoking the failure.
	 */
	public void setFailureResult(Exception e){
		this.setResult(null);
		this.callFailed= true;
		this.setFailureException(e);
	}
	/**
	 * Indicated the state of the asyncresult object.
	 * @return whether the invocation failed or not.
	 */
	public boolean isFailed(){
		return callFailed;
	}
	
	/**
	 * Allows obtaining the progress handler established to the asyncresult objects.
	 * @return the progress handler object. 
	 */
	public ProgressHandler getProgressHandler(){
		return progressHandler;
	}
	
	/**
	 * Sets the exception that occur when a failure in the invocation of the web method happens.
	 * @param failureException
	 */
	public void setFailureException(Exception failureException) {
		this.failureException = failureException;
	}
	
	/**
	 * Returns the Exception object associated to the failure . 
	 * @return
	 */
	public Exception getFailureException() {
		return failureException;
	}
	
	/**
	 * Allows setting up the result of the invocation of the web method.
	 * @param result The result of the operation. 
	 */
	public void setResult(Result result) {
		this.result = result;
	}
	/**
	 * Allows getting the result of the operation after invoking a web method.
	 * @return the result of the operation.
	 */
	public Result getResult() {
		return result;
	} 

} 
