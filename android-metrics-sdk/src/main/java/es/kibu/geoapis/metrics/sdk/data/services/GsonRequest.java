package es.kibu.geoapis.metrics.sdk.data.services;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.kibu.geoapis.serialization.GsonUtils;


/**
 * Created by lrodriguez2002cu on 26/07/2016.
 */

public class GsonRequest<T, P, I> extends Request<T> {

    public static final String TAG = "GsonRequest";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String NO_DESERIALIZER_IS_CONFIGURED = "No deserializer is configured";
    private final Gson gson = GsonUtils.getGson();

    private final Class<T> clazz;
    private final Response.Listener<T> listener;
    private final P params;

    Type typeToken = new TypeToken<List<I>>() {
    }.getType();

    ResponseDeserializer deserializer;
    private Map<String, String> headers;
    private Class<I> collectionItemType;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url     URL of the request to make
     * @param clazz   Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     */
    protected GsonRequest(int method, String url, Class<T> clazz, Map<String, String> headers,
                          Response.Listener<T> listener, Response.ErrorListener errorListener, Class<I> collectionItemType) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.listener = listener;
        this.params = null;
        this.collectionItemType = collectionItemType;

        //initHeaders(headers);
    }

    protected GsonRequest(int method, String url, Class<T> clazz, P param, Map<String, String> headers,
                          Response.Listener<T> listener, Response.ErrorListener errorListener, Class<I> collectionItemType) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.headers = headers;
        this.listener = listener;
        this.params = param;
        this.collectionItemType = collectionItemType;

        //initHeaders(headers);
    }

    public static <K> GsonRequest post(String url, Class<K> clazz, Map<String, String> headers,
                                       Response.Listener<K> listener, Response.ErrorListener errorListener) {
        return new GsonRequest(Method.POST, url, clazz, headers,
                listener, errorListener, null);
    }


//    public static <K> GsonRequest get (String url, Class<K> clazz, Object param, Map<String, String> headers,
//                       Response.Listener<K> listener, Response.ErrorListener errorListener, Class collectionItemType){
//          return new GsonRequest(Method.GET, url, clazz, param, headers,
//                  listener, errorListener, collectionItemType);
//    }

    /*public static <K> GsonRequest get(String url, Class<K> clazz, Map<String, String> headers,
                                      Response.Listener<K> listener, Response.ErrorListener errorListener) {
        return new GsonRequest(Method.GET, url, clazz, headers,
                listener, errorListener, null);
    }*/

    public static <T, P, I> GsonRequest post(String url, Class<T> clazz, P param, Map<String, String> headers,
                                             Response.Listener<T> listener, Response.ErrorListener errorListener, Class<I> collectionType) {
        return new GsonRequest(Method.POST, url, clazz, param, headers,
                listener, errorListener, collectionType);
    }

    public static <T, P> GsonRequest post(String url, Class<T> clazz, P param, Map<String, String> headers,
                                          Response.Listener<T> listener, Response.ErrorListener errorListener) {
        return new GsonRequest(Method.POST, url, clazz, param, headers,
                listener, errorListener, null);
    }

    /*public static <T, *//*P,*//* I> GsonRequest get(String url, Class<T> resultClazz, *//*P param, *//*Map<String, String> headers,
                                         Response.Listener<T> listener, Response.ErrorListener errorListener, Class<I> collectionType) {
        return new GsonRequest(Method.GET, url, resultClazz, *//*param, *//*headers,
                listener, errorListener, collectionType);
    }
*/
    public static <T> GsonRequest get(String url, Class<T> resultClazz, Map<String, String> headers,
                                      Response.Listener<T> listener, Response.ErrorListener errorListener) {
        return new GsonRequest(Method.GET, url, resultClazz, /*param,*/ headers,
                listener, errorListener, null);
    }

    public static <K, I> GsonRequest get(String url, Class<K> clazz, Map<String, String> headers,
                                         Response.Listener<K> listener, Response.ErrorListener errorListener, Class<I> collectionType) {
        return new GsonRequest(Method.GET, url, clazz, headers,
                listener, errorListener, collectionType);
    }

    /*
    protected GsonRequest(int method, String url, Class<T> clazz, P param, Map<String, String> headers,
                          Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(method, url, clazz, param, headers, listener, errorListener, null);
    }
*/
    private void initHeaders(Map<String, String> headers) {
        if (headers == null || !headers.containsKey(CONTENT_TYPE)) {
            this.headers = new HashMap<>();
            this.headers.put(CONTENT_TYPE, "application/json");
        } else {
            this.headers = headers;
        }
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (this.getMethod() == Method.GET) {
            return null;
        }
        byte[] bytes = null/*(params == null) ? null : json.getBytes()*/;

        //In case the param is a string.. it will be considered the 'raw' content to be sent
        if (params instanceof String) {
            Log.d(TAG, "[BODY]raw object will be sent: " + ((String) params));
            bytes = ((String) params).getBytes();
        } else {
            String json = gson.toJson(params);
            Log.d(TAG, "[BODY]object serialized as: " + json);
            bytes = (params == null) ? null : json.getBytes();
        }

        String message = String.format("Body: %s", bytes != null ? new String(bytes) : "<empty>");
        Log.d(TAG, message);

        return bytes;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    //In your extended request class
    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {

        if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {

            try {
                String json = new String(
                        volleyError.networkResponse.data,
                        HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));

                Exception responseException = getResponseException(json, clazz);
                return new VolleyError(responseException);

            } catch (UnsupportedEncodingException | JsonSyntaxException e) {
                Log.e(TAG, "parseNetworkError: " + e.getMessage(), e);
            }

            String exceptionMessage = new String(volleyError.networkResponse.data);
            Log.d(TAG, "parseNetworkError - non json error: " + exceptionMessage);
            VolleyError error = new VolleyError(exceptionMessage);

            volleyError = error;
        }
        return volleyError;
    }

    public Type getTypeToken() {

        if (clazz.equals(List.class)) {
          /*  if (collectionItemType.equals(GameDescription.class)) return  new TypeToken<List<GameDescription>>() {}.getType();
            if (collectionItemType.equals(GameStatus.class)) return  new TypeToken<List<GameStatus>>() {}.getType();*/

            throw new RuntimeException("Unregistered type token");
        } else throw new RuntimeException("Unregistered type token");

        //return typeToken;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));


            Log.d("JSON_RESPONSE", String.format("[%d] %s", response.statusCode, json));
            if (response.statusCode == 200) {
                Log.d("JSON_SUCCSS", String.format("[%d] %s", response.statusCode, json));

                T result = getResponseDeserialized(json, clazz);

                return Response.success(
                        result,
                        HttpHeaderParser.parseCacheHeaders(response));
            } else {
                Log.d("JSON_ERROR", String.format("[%d] %s", response.statusCode, json));
                Exception cause = getResponseException(json, clazz);
                Response<T> error = Response.error(new VolleyError(cause));
                return error;
            }

        } catch (UnsupportedEncodingException e) {
            Log.e("ENCODING_ERROR", e.getMessage(), e);
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException ex) {
            Log.e("JSON_ERROR", ex.getMessage(), ex);
            return Response.error(new ParseError(ex));
        }
    }

    private Exception getResponseException(String json, Class<T> clazz) {
        ResponseDeserializer deserializer = getDeserializer();
        if (deserializer != null) {
            return deserializer.getResponseException(json, clazz);
        }
        throw new RuntimeException(NO_DESERIALIZER_IS_CONFIGURED);
    }

    private T getResponseDeserialized(String json, Class<T> clazz) {
        ResponseDeserializer deserializer = getDeserializer();
        if (deserializer != null) {
            return deserializer.getResponseDeserialized(json, clazz);
        }
        throw new RuntimeException(NO_DESERIALIZER_IS_CONFIGURED);
    }

    public ResponseDeserializer getDeserializer() {
        return deserializer;
    }

    public GsonRequest withDeserializer(ResponseDeserializer deserializer) {
        this.deserializer = deserializer;
        return this;
    }


    interface ResponseDeserializer {
        Exception getResponseException(String json, Class<?> itemClass);

        <T> T getResponseDeserialized(String json, Class<?> itemClass);
    }
}