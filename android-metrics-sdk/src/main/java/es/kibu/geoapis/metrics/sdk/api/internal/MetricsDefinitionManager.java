package es.kibu.geoapis.metrics.sdk.api.internal;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.util.Map;

import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.metrics.MetricsCoreUtils;
import es.kibu.geoapis.metrics.sdk.data.services.AsyncResult;
import es.kibu.geoapis.metrics.sdk.data.services.MetricsAsyncServiceClient;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

/**
 * Created by lrodriguez2002cu on 10/01/2017.
 */
public class MetricsDefinitionManager {

    public static final String TAG = MetricsDefinitionManager.class.getSimpleName();

    static MetricsDefinitionManager manager;

    Configurable config;
    Context context;
    MetricsAsyncServiceClient client;
    DefinitionManagerStore store;
    MetricsDefinitionResolver resolver = new MetricsDefinitionResolver(client, getAsyncResultForResolver());
    Map<String, MetricsResult> metricsResults;
    MetricsDefinitionManagerEvents eventHandler;

    protected MetricsDefinitionManager(Configurable config, Context context, MetricsAsyncServiceClient client, MetricsDefinitionManagerEvents eventHandler) {

        assert config != null;
        //assert client != null;
        assert context != null;

        this.config = config;
        this.context = context;
        this.client = client;
        resolver = new MetricsDefinitionResolver(client, getAsyncResultForResolver());
        this.store = new DefaultDefinitionManagerStore();
        this.eventHandler = eventHandler;

        loadStoredDefs();
        retrieveMetricsDefinition();
    }

    public static MetricsDefinitionManager getInstance() {
        if (manager == null) {
            throw new RuntimeException("Init must be called first on MetricsDefinitionManager");
        }
        return manager;
    }

    public static MetricsDefinitionManager init(Configurable config, Context context,
                                                MetricsAsyncServiceClient client, MetricsDefinitionManagerEvents eventHandler) {
        manager = new MetricsDefinitionManager(config, context, client, eventHandler);

        return manager;
    }

    public MetricsDefinitionResolver getResolver() {
        return resolver;
    }

    public void setResolver(MetricsDefinitionResolver resolver) {
        this.resolver = resolver;
    }

    Map<String, MetricsResult> getMetricsDefMap() {
        return metricsResults;
    }

    public AsyncResult<MetricsResult> getAsyncResultForResolver() {
        return new ResolverAsyncResult(this);
    }

    private void retrieveMetricsDefinition() {
        resolver.solve(config.getApplication());
    }

    private void loadStoredDefs() {
        Log.d(TAG, "loading definitions from storage");
        metricsResults = store.loadAll();
        Log.d(TAG, String.format("loaded %d definitions from storage: %s", metricsResults.size(), metricsResults.keySet()));
    }

    public MetricsDefinition getMetricsDefinition(String applicationId) {
        try {
            //TODO: verify that it is saved for that application
            MetricsResult metricsResult = metricsResults.get(applicationId);
            if (metricsResult == null) {
                return null;
            }
            return MetricsCoreUtils.withMetricDefinitionFromContent(metricsResult.getContent());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

   /* public Future<MetricsDefinition> getMetricsDefinitionAsync(String applicationId) {

    }*/

    public void reload() {
        reloadStored();
        retrieveMetricsDefinition();
    }

    private void reloadStored() {
        metricsResults.clear();
        loadStoredDefs();
    }

    public void clearAll() {
        store.clearAll();
        metricsResults.clear();
    }

    public void registerEventHandler(MetricsDefinitionManagerEvents eventHandler) {
        this.eventHandler = eventHandler;
    }

    public void notifyDefinitionUnavailable(){
        if (this.eventHandler!= null) {
            this.eventHandler.onMetricsDefinitionRetrieveFailure(this.config.getApplication());
        }
    }

    public void notifyDefinitionAvailable(){
        if (this.eventHandler!= null) {
            String application = config.getApplication();
            MetricsDefinition definition = getMetricsDefinition(application);
            this.eventHandler.onMetricsDefinitionAvailable(application, definition);
        }
    }

    Handler retryHandler;

    public void retryReload(long millis) {
        if (retryHandler == null) {
            retryHandler = new Handler();
        }
        else cancelRetry();

        retryHandler.postDelayed(new Runnable(){
            @Override
            public void run(){
                reload();
            }
        }, millis);
    }

    public void cancelRetry() {
        if (retryHandler!= null) {
            retryHandler.removeCallbacksAndMessages(null);
        }
    }


    public interface MetricsDefinitionManagerEvents {

        void onMetricsDefinitionAvailable(String application, MetricsDefinition metricsDefinition);

        void onMetricsDefinitionRetrieveFailure(String application);
    }

    static class ResolverAsyncResult extends AsyncResult<MetricsResult> {

        MetricsDefinitionManager manager;

        public ResolverAsyncResult() {
            super(null);
        }

        public ResolverAsyncResult(MetricsDefinitionManager manager) {

            super(null);
            assert manager != null;
            this.manager = manager;
        }

        @Override
        public void onFailure(Exception ex) {
            Log.e(TAG, "Failure while retrieving the definition", ex);
            manager.notifyDefinitionUnavailable();
        }

        @Override
        public void onSuccess(MetricsResult metricsResult) {
            if (manager != null) {
                String application = manager.config.getApplication();
                Log.d(TAG, "Saving definition...");
                manager.store.saveOrUpdateMetricsResults(metricsResult, application);

                Log.d(TAG, "Reloading metrics definitions...");
                manager.reloadStored();
                manager.notifyDefinitionAvailable();
            }

            Log.d(TAG, "Definition retrieved successfully");
        }
    }

    public static class MetricsDefinitionResolver {

        public static String TAG = "MDefResolver";

        MetricsAsyncServiceClient client;
        AsyncResult<MetricsResult> result;

        public MetricsDefinitionResolver(MetricsAsyncServiceClient client, AsyncResult<MetricsResult> result) {
            this.client = client;
            this.result = result;
        }

        public void solve(String application) {
            Log.d(TAG, "Resolving metric for application:" + application);

            if (client != null) {
                Log.d(TAG, "Resolving through client for app: " + application);
                client.metricsAsync(application, result);
            } else Log.d(TAG, "No client specified" + application);
        }
    }
}
