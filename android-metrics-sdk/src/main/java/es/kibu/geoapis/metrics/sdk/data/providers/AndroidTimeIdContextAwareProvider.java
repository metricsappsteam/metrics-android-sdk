package es.kibu.geoapis.metrics.sdk.data.providers;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.data.providers.DataProvider;
import es.kibu.geoapis.objectmodel.dimensions.Constants;

/**
 * Created by lrodriguez2002cu on 03/01/2017.
 */

public class AndroidTimeIdContextAwareProvider implements DataProvider<String> {

    public static final String TAG = "TimeIdCtxAwareProvider";

    Map<String, AndroidTimeIdProvider> providersMap = new HashMap();

    private String getKey(DataContext ctx){
        //ctx.getDimension().getName()
        return String.format("%s-%s", ctx.getDefinition().getId(), ctx.getVariable().getName());
    }

    @Override
    public String getNext(DataContext dataContext) {
        AndroidTimeIdProvider timeIdProvider = getAndroidTimeIdProvider(dataContext);

        boolean isNext = dataContext.getDimension().getName().equalsIgnoreCase(Constants.TIME_ID);
        String isNextStr = (isNext) ? "CURRENT" : "PREV";

        String value = "";
        if (isNext) {
            value = timeIdProvider.getNext(dataContext);
        } else value = timeIdProvider.getPrev(dataContext);
        //Log.d(TAG, String.format("%s (%s): %s", isNextStr, dataContext.getDimension().getName(), value));

        return value;
    }

/*  private AndroidTimeIdProvider getTimeIdProvider(DataContext dataContext) {
        AndroidTimeIdProvider provider = getAndroidTimeIdProvider(dataContext);
        return provider;
    }*/

    private AndroidTimeIdProvider getAndroidTimeIdProvider(DataContext dataContext) {
        String key = getKey(dataContext);
        if (!providersMap.containsKey(key)) {
            providersMap.put(key, new AndroidTimeIdProvider());
        }
        return providersMap.get(key);
    }
}
