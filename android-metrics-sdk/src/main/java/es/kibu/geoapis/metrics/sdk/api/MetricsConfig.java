package es.kibu.geoapis.metrics.sdk.api;

/**
 * Created by lrodriguez2002cu on 08/11/2016.
 */

import es.kibu.geoapis.api.Configurable;

/**
 * Provides the configuration to the MetricsAPI needed for submitting data.
 */
public interface MetricsConfig extends Configurable {

}
