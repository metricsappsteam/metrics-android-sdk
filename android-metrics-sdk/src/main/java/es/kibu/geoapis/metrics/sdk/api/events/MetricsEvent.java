package es.kibu.geoapis.metrics.sdk.api.events;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 12/01/2017.
 */
public interface MetricsEvent extends Serializable{
    String DATA = "data";
    String TOPIC = "topic";
    String getData();
}
