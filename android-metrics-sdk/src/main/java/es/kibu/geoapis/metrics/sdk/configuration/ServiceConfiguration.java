package es.kibu.geoapis.metrics.sdk.configuration;

import static es.kibu.geoapis.metrics.sdk.configuration.ManifestConfigurationReader.METRICS_BASE_KEY;

/**
 * Created by lrodriguez2002cu on 10/01/2017.
 */

/**
 * Reads the configuration used for accessing the services.
 * The keys in the metadata useds at this point are:
 *  metrics-service-url: which contains the base url for the services (ex. http://localhost:9000).
 */
public class ServiceConfiguration {

    Configuration configuration;

    public static final String SERVICE_URL = "service-url";

    private String getKey(String partialKey){
        return METRICS_BASE_KEY + partialKey;
    }

    public ServiceConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Returns the base url for accesing the services.
     *
     * @return
     */
    public String getBaseURL() {
        String value = configuration.get(getKey(SERVICE_URL));
        return value;
    }
}
