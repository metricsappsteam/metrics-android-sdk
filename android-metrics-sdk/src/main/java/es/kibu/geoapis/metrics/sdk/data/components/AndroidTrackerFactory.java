package es.kibu.geoapis.metrics.sdk.data.components;

import dagger.Component;
import es.kibu.geoapis.api.Tracker;
import es.kibu.geoapis.metrics.sdk.data.modules.AndroidProvidersModule;
import es.kibu.geoapis.metrics.sdk.data.modules.AndroidTrackerModule;
import es.kibu.geoapis.metrics.sdk.data.modules.DataSubmitterModule;

/**
 * Created by lrodriguez2002cu on 08/11/2016.
 */


@Component (modules = {AndroidTrackerModule.class, AndroidProvidersModule.class, DataSubmitterModule.class})
public interface AndroidTrackerFactory {
     Tracker createTracker();
}
