package es.kibu.geoapis.metrics.sdk.api;

import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.squareup.otto.Bus;

import es.kibu.geoapis.api.MetricsApi;
import es.kibu.geoapis.api.Tracker;
import es.kibu.geoapis.metrics.sdk.api.events.MetricsBroadcastReceiver;
import es.kibu.geoapis.metrics.sdk.api.events.MetricsEventsReceiver;
import es.kibu.geoapis.metrics.sdk.api.internal.MetricsDefinitionManager;
import es.kibu.geoapis.metrics.sdk.configuration.ConfigurationManager;
import es.kibu.geoapis.metrics.sdk.configuration.ServiceConfiguration;
import es.kibu.geoapis.metrics.sdk.data.components.AndroidTrackerFactory;
import es.kibu.geoapis.metrics.sdk.data.components.DaggerAndroidTrackerFactory;
import es.kibu.geoapis.metrics.sdk.data.history.RetryData;
import es.kibu.geoapis.metrics.sdk.data.modules.AndroidProvidersModule;
import es.kibu.geoapis.metrics.sdk.data.modules.AndroidTrackerModule;
import es.kibu.geoapis.metrics.sdk.data.modules.DataSubmitterModule;
import es.kibu.geoapis.metrics.sdk.data.services.DefaultMetricsAsyncServiceClient;
import es.kibu.geoapis.metrics.sdk.data.services.MetricsAsyncServiceClient;
import es.kibu.geoapis.metrics.sdk.fcm.FCMRegisterer;
import es.kibu.geoapis.notifications.TopicHandler;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

import static es.kibu.geoapis.metrics.sdk.api.events.MetricsBroadcastReceiver.METRICS_MESSAGE_ACTION;

/**
 * Created by lrodriguez2002cu on 07/11/2016.
 */

public class AndroidMetricsApi extends MetricsApi {

    public static final String TAG = "AndroidMetricsApi";
    public static final String METRICS_BUS = "metrics-bus";
    public static final int METRICS_DEF_RETRIEVALRETRY_MILLIS = 5000;
    protected MetricsDefinitionManager manager;
    MetricsBroadcastReceiver receiver;
    private Context context;
    private FCMRegisterer gcmRegisterer;
    private Bus eventBus;
    private MetricsAsyncServiceClient client;
    private boolean defaultTrackerCreated = false;

    MetricsDefinitionManager.MetricsDefinitionManagerEvents events = new MetricsDefinitionManager.MetricsDefinitionManagerEvents() {
        @Override
        public void onMetricsDefinitionAvailable(String application, MetricsDefinition metricsDefinition) {
            manager.cancelRetry();
            if (AndroidMetricsApi.this.defaultTrackerCreated) {
                Tracker defaultTracker = AndroidMetricsApi.this.getDefaultTracker();
                if (defaultTracker.getMetricsDefinition() == null) {
                    defaultTracker.setMetricsDefinition(metricsDefinition);
                }
            }
        }

        @Override
        public void onMetricsDefinitionRetrieveFailure(String application) {
            Log.e(TAG, "Api is unable to retrieve metrics definition for: " + application
                    + " retrying in " + METRICS_DEF_RETRIEVALRETRY_MILLIS + "  milliseconds");

            manager.retryReload(METRICS_DEF_RETRIEVALRETRY_MILLIS);
        }
    };

    protected AndroidMetricsApi(Context context, MetricsConfig config) {
        super(config);
        assert config != null;
        assert context != null;

        this.context = context;

        createClient();
        initManager();
        initMessaging();
    }

    public static boolean isTesting() {
        return testing;
    }

    public static void setTesting(boolean testing) {
        MetricsApi.testing = testing;
    }

    /**
     * Initializes the api with the context and a configuration
     *
     * @param context the context
     * @param config  a configuration providing information about the user, application and session
     */
    public static void init(Context context, MetricsConfig config) {

        Log.d(TAG, String.format("Initializing Metrics API {app: %s, session: %s, user: %s}",
                config.getApplication(), config.getSession(), config.getUser()));

        Configuration configuration = new Configuration.Builder(context)
                .addModelClass(RetryData.class)
                .addModelClass(es.kibu.geoapis.metrics.sdk.api.internal.DefinitionResultRecord.class)
                .addTypeSerializer(es.kibu.geoapis.metrics.sdk.data.history.ActiveAndroidSerializers.DateTimeSerializer.class)
                .setDatabaseName("metrics.db")
                .create();

        ActiveAndroid.initialize(configuration, true);

        api = new AndroidMetricsApi(context, config);

        Log.d(TAG, "Metrics API initialized successfully");

    }

    /**
     * Allows registering an event receiver for receiving notifications through an event bus.
     *
     * @param receiver
     */

    public static void registerReceiver(MetricsEventsReceiver receiver) {
        ((AndroidMetricsApi) getInstance()).internalRegisterMessagesReceiver(receiver);
    }

    public static void unregisterReceiver(MetricsEventsReceiver receiver) {
        ((AndroidMetricsApi) getInstance()).internalUnregisterReceiver(receiver);
    }

    public Context getContext() {
        return context;
    }

    protected MetricsDefinition getMetricsDefinition() {
        return manager.getMetricsDefinition(config.getApplication());
    }

    /**
     * Initializes the push-messaging capabilities of the application.
     */
    private void initMessaging() {

        assert config != null;
        assert context != null;

        /*<string name="gcm_sender_id">898312206461</string>
        <!--For server usage-->
        <string name="gcm_api_key">AIzaSyAGg_chdXWm9Q1QmHfmXReLo8AbyQoZVVQ</string>
        */
        Log.d(TAG, "Creating " + METRICS_BUS);
        eventBus = new Bus(METRICS_BUS);
        //String gcmSenderId = ConfigurationManager.getInstance(context).getConfiguration().get("metrics-gcm-sender-id");
        //String gcmKey = ConfigurationManager.getInstance(context).getConfiguration().get("metrics-gcm-key");

        if (!isTesting()) {
            Log.d(TAG, "Creating gcmRegisterer");
            gcmRegisterer = new FCMRegisterer(context, TopicHandler.getTopics(config));
        }

        Log.d(TAG, "Creating broadcast receiver");
        receiver = new MetricsBroadcastReceiver(eventBus);
        IntentFilter filter = new IntentFilter(METRICS_MESSAGE_ACTION);
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);

    }

    private void initManager() {
        manager = MetricsDefinitionManager.init(config, getContext(), client, events);
    }

    //region Registry Methods

    private void createClient() {
        ConfigurationManager configurationManager = ConfigurationManager.getInstance(getContext());
        ServiceConfiguration serviceConfiguration = configurationManager.getServicesConfiguration();
        String baseUrl = serviceConfiguration.getBaseURL();
        this.client = new DefaultMetricsAsyncServiceClient(baseUrl, getContext());
    }

    @Override
    protected Tracker getTracker(boolean forTesting) {
        AndroidTrackerFactory factory = DaggerAndroidTrackerFactory.builder()
                .androidProvidersModule(new AndroidProvidersModule(getContext(), config, forTesting))
                .androidTrackerModule(new AndroidTrackerModule())
                .dataSubmitterModule(new DataSubmitterModule(getContext(), config, eventBus, forTesting))
                .build();
        defaultTrackerCreated = true;
        return factory.createTracker();
    }

    private void internalRegisterMessagesReceiver(MetricsEventsReceiver appEvtReceiver) {
        eventBus.register(appEvtReceiver);
    }

    private void internalUnregisterReceiver(MetricsEventsReceiver receiver) {
        eventBus.unregister(receiver);
    }
    //endregion
}
