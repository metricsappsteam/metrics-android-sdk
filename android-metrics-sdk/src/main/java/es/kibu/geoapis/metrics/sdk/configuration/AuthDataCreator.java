package es.kibu.geoapis.metrics.sdk.configuration;


/**
 * Created by lrodriguez2002cu on 15/05/2016.
 */
public class AuthDataCreator {


    public static final String FENCES_APP_ACCESS_TOKEN = "fences-app-access-token";
    public static final String FENCES_API_KEY = "fences-api-key";
    public static final String FENCES_API_SECRET = "fences-api-secret";

    /*public static AuthConfiguration getAuthConfiguration(Configuration configuration) {

        AuthConfiguration authConfiguration = new AuthConfiguration();

        String appAccessToken = configuration.get(FENCES_APP_ACCESS_TOKEN);
        ApplicationAccessToken applicationToken = new ApplicationAccessToken(appAccessToken);
        authConfiguration.setApplicationToken(applicationToken);

        String apiKey = configuration.get(FENCES_API_KEY);
        String apiSecret = configuration.get(FENCES_API_SECRET);

        authConfiguration.setApiKeyAndSecret(new APIKeyAndSecret(apiKey, apiSecret));

        return  authConfiguration;
    }*/
}
