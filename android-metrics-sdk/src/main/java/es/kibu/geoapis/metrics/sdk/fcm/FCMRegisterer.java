package es.kibu.geoapis.metrics.sdk.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.kibu.geoapis.metrics.sdk.R;

/**
 * Created by lrodriguez2002cu on 13/01/2017.
 */

public class FCMRegisterer {

    public static final String TAG = "FCMRegisterer";
    public static final String TOPIC_LIST = "topicList";

    Context context;

    public FCMRegisterer(Context context, List<String> topics) {

        assert topics!= null && topics.size()>0;
        this.context = context;

        Log.d(TAG, String.format("Registering topics %s", topics));

        Set<String> diffTopics = saveTopicsAndExtractTopicsToRemove(topics);

        for (String oldTopic : diffTopics) {
            Log.d(TAG, String.format("Unsubscribing from topic %s", oldTopic));
            FirebaseMessaging.getInstance().unsubscribeFromTopic(oldTopic);
        }

        for (String topic : topics) {
            Log.d(TAG, String.format("Subscribing to topic %s", topic));
            FirebaseMessaging.getInstance().subscribeToTopic(topic);
        }
        //init(context, gcmKey, topics);
    }

    private Set<String> listToSet(  List<String> list) {
        Set<String> set = new HashSet<String>(list);
        for (String s : list) {
            set.add(s);
        }
        return set;
    }

    private Set<String> saveTopicsAndExtractTopicsToRemove(List<String> topics){

        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.topics), Context.MODE_PRIVATE);

        Set<String> diffTopicsSet = new HashSet<>();
        Set<String> newTopicsSet = listToSet(topics);

        if (sharedPref.contains(TOPIC_LIST)) {
            Set<String> oldTopicsSet = sharedPref.getStringSet(TOPIC_LIST,  null);
            //compute the difference
            if (oldTopicsSet!= null) {
                for (String oldTopic : oldTopicsSet) {
                    if (!newTopicsSet.contains(oldTopic)){
                        diffTopicsSet.add(oldTopic);
                    }
                }
            }
        }

        sharedPref.edit().clear().putStringSet(TOPIC_LIST, newTopicsSet).commit();

        return  diffTopicsSet;
    }

}
