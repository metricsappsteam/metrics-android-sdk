package es.kibu.geoapis.metrics.sdk.fcm;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import es.kibu.geoapis.metrics.sdk.api.events.MetricsEvent;

import static es.kibu.geoapis.metrics.sdk.api.events.MetricsBroadcastReceiver.METRICS_MESSAGE_ACTION;

public class MetricsSDKMessagingService extends FirebaseMessagingService {
    public static final String TAG = "MSDKMessagingService";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, String.format("created: %s", "MetricsSDKMessagingService"));
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "to: " + remoteMessage.getTo());
        Log.d(TAG, "Message type: " + remoteMessage.getMessageType());
        Log.d(TAG, "Collapse key: " + remoteMessage.getCollapseKey());
        Log.d(TAG, "MessageId: " + remoteMessage.getMessageId());
        Log.d(TAG, "Sent Time: " + remoteMessage.getSentTime());
        Log.d(TAG, "Data: " + remoteMessage.getData());

        Log.d(TAG, "Notification Message Body: " + ((remoteMessage.getNotification()!= null)? remoteMessage.getNotification().getBody(): "<null body>"));

        String message = remoteMessage.getData().get("message");
        //"/topics/app-45519f89e1bb4c4b~session1~user1"
        String topic = remoteMessage.getFrom().replace("/topics/", "");

        Intent intent = new Intent();
        intent.setAction(METRICS_MESSAGE_ACTION);
        intent.putExtra(MetricsEvent.DATA, message);
        intent.putExtra(MetricsEvent.TOPIC, topic);

        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
        //this.sendBroadcast(intent);

    }
}
