package es.kibu.geoapis.metrics.sdk.api.events;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.squareup.otto.Bus;

import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.metrics.sdk.api.AndroidMetricsApi;
import es.kibu.geoapis.metrics.sdk.api.MetricsConfig;
import es.kibu.geoapis.notifications.TopicHandler;


/**
 * Created by lrodriguez2002cu on 12/01/2017.
 */

public class MetricsBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "MBroadcastReceiver";
    public static final String METRICS_MESSAGE_ACTION = "es.kibu.geoapis.metrics.sdk.METRICS_MESSAGE";

    Bus bus;

    public MetricsBroadcastReceiver(Bus bus) {
        this.bus = bus;
        if (bus == null) throw new RuntimeException("bus must not be null in the MetricsBroadcastReceiver");
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        //final PendingResult pendingResult = goAsync();
        String data = intent.getStringExtra(MetricsEvent.DATA);
        String topic = intent.getStringExtra(MetricsEvent.TOPIC);
        TopicHandler.TOPIC_TYPE topic_type = TopicHandler.decodeTopic(topic);

        Log.d(TAG, String.format("Dispatching event through event bus %s", topic_type.name()));

        Configurable config = AndroidMetricsApi.getInstance().getConfig();

        switch (topic_type) {
            case APPLICATION:
                if (config.getApplication()
                        .equalsIgnoreCase(topic_type.getApplication())) {
                    bus.post(new ApplicationEvent(data));
                }
                else Log.d("TAG", String.format("TopicType with invalid parameters: %s ",  topic_type.toString()));
                break;
            case SESSION:
                if (config.getApplication()
                        .equalsIgnoreCase(topic_type.getApplication())
                        && config.getSession().equalsIgnoreCase(topic_type.getSession())) {
                    bus.post(new SessionEvent(data));
                }
                else Log.d("TAG", String.format("TopicType with invalid parameters: %s ",  topic_type.toString()));

                break;
            case USER:
                if (config.getApplication()
                        .equalsIgnoreCase(topic_type.getApplication())
                        && config.getSession().equalsIgnoreCase(topic_type.getSession())
                        && config.getUser().equalsIgnoreCase(topic_type.getUser())) {

                    bus.post(new UserEvent(data));
                }
                else Log.d("TAG", String.format("TopicType with invalid parameters: %s ",  topic_type.toString()));
                break;
        }

    }
}
