package es.kibu.geoapis.metrics.sdk.data.providers;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedGenerator;

import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.data.providers.DataProvider;

/**
 * Created by lrodriguez2002cu on 01/12/2016.
 */

public class AndroidTimeIdProvider implements DataProvider<String> {

    String previous;
    String current;
    AndroidTimeIdProvider   providerToWrap;

    TimeBasedGenerator gen = Generators.timeBasedGenerator(EthernetAddress.fromInterface());
    private PrevProviderWrapper prevProviderWrapper;

    public AndroidTimeIdProvider (AndroidTimeIdProvider providerToWrap) {
        this.providerToWrap = providerToWrap;
    }

    public AndroidTimeIdProvider () {
        this.providerToWrap = null;
    }

    @Override
    public String getNext(DataContext context) {
        previous = current;
        current = (providerToWrap == null)?  gen.generate().toString(): providerToWrap.getNext(context);
        return current;
    }

    public String getPrev(DataContext context){
        return previous;
    }

    public DataProvider<String> getProviderWrappper () {
        if (prevProviderWrapper == null){
            prevProviderWrapper = new PrevProviderWrapper(this);
        }
        return prevProviderWrapper;
    }

    public static class PrevProviderWrapper implements DataProvider<String> {

        AndroidTimeIdProvider timeIdProvider;

        public PrevProviderWrapper(AndroidTimeIdProvider timeIdProvider) {
            this.timeIdProvider = timeIdProvider;
        }

        @Override
        public String getNext(DataContext context) {
            return timeIdProvider.getPrev(context);
        }
    }


}
