package es.kibu.geoapis.metrics.sdk.data.modules;

import android.content.Context;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.data.providers.BooleanProvider;
import es.kibu.geoapis.data.providers.DataProvider;
import es.kibu.geoapis.data.providers.DoubleProvider;
import es.kibu.geoapis.data.providers.IntegerProvider;
import es.kibu.geoapis.data.providers.LocationProvider;
import es.kibu.geoapis.data.providers.OrientationProvider;
import es.kibu.geoapis.data.providers.StringProvider;
import es.kibu.geoapis.data.providers.TimeDataProvider;
import es.kibu.geoapis.metrics.sdk.api.MetricsConfig;
import es.kibu.geoapis.metrics.sdk.data.providers.AndroidLocationProvider;
import es.kibu.geoapis.metrics.sdk.data.providers.AndroidOrientationProvider;
import es.kibu.geoapis.metrics.sdk.data.providers.AndroidTimeIdContextAwareProvider;
import es.kibu.geoapis.metrics.sdk.data.providers.AndroidTimeIdProvider;
import es.kibu.geoapis.metrics.sdk.data.providers.ProviderException;
import es.kibu.geoapis.objectmodel.dimensions.Constants;

/**
 * Created by lrodriguez2002cu on 08/11/2016.
 */
@Module
public class AndroidProvidersModule {

    public static final String THIS_IS_DISABLED_IN_ANDROID = "This is disabled in android";

    Context context;
    Configurable config;

    private AndroidTimeIdContextAwareProvider timeIdContextAwareProvider;

    public AndroidTimeIdContextAwareProvider getTimeIdContextAwareProvider() {
        if (timeIdContextAwareProvider == null) {
            timeIdContextAwareProvider = new AndroidTimeIdContextAwareProvider();
        }
        return timeIdContextAwareProvider;
    }

    public AndroidProvidersModule(Context context, Configurable config, boolean testing) {
        this.context = context;
        this.config = config;
        this.testing = testing;
    }

    boolean testing = false;

    @Provides
    public DataProvider<DataUtils.Location> provideLocationData() {
        return (testing) ? new LocationProvider() : new AndroidLocationProvider(context);
    }

    @Provides
    public DataProvider<DataUtils.Orientation> provideOrientation() {
        return (testing) ? new OrientationProvider() : new AndroidOrientationProvider(this.context);
    }

    @Provides
    @Named(Constants.TIME)
    public DataProvider<String> provideTime() {
        return new TimeDataProvider();
    }

    @Provides
    public DataProvider<Integer> provideInt() {
        return new IntegerProvider();
        //throw new ProviderException("This is disabled in android");
    }

    @Provides
    public DataProvider<Double> provideDouble() {
        //throw new ProviderException("This is disabled in android");
        return new DoubleProvider();
    }

    @Provides
    public DataProvider<String> provideString() {
        //throw new ProviderException("This is disabled in android");
        return new StringProvider();
    }

    @Provides
    public DataProvider<Boolean> provideBoolean() {
        //throw new ProviderException(THIS_IS_DISABLED_IN_ANDROID);
        return new BooleanProvider();
    }

    @Provides
    @Named(Constants.USER)
    public DataProvider<String> provideUser() {
        return new DataProvider<String>() {
            @Override
            public String getNext(DataContext context) {
                return config.getUser();
            }
        };
    }

    @Provides
    @Named(Constants.TIME_ID)
    public DataProvider<String> provideTimeId() {
        return getTimeIdContextAwareProvider();
    }

    @Provides
    @Named(Constants.PREV_ID)
    public DataProvider<String> providePrevId() {
        return getTimeIdContextAwareProvider();
    }

    @Provides
    @Named(Constants.APPLICATION)
    public DataProvider<String> provideApplication() {
        return new DataProvider<String>() {
            @Override
            public String getNext(DataContext context) {
                return config.getApplication();
            }
        };
    }

    @Provides
    @Named(Constants.SESSION)
    public DataProvider<String> provideSession() {
        return new DataProvider<String>() {
            @Override
            public String getNext(DataContext context) {
                return config.getSession();
            }
        };
    }

}
