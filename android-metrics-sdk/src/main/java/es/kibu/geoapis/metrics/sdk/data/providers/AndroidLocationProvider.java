package es.kibu.geoapis.metrics.sdk.data.providers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import org.joda.time.DateTime;

import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.data.providers.DataProvider;

/**
 * Created by lrodriguez2002cu on 07/11/2016.
 */

public class AndroidLocationProvider implements DataProvider<DataUtils.Location> {

    public static final String TAG = "ALocationProvider";

    Context context;

    public AndroidLocationProvider(Context context) {
        //Context baseContext = app.getBaseContext();
        this.context = context;
        init();
    }

    void init() {
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);

        //String locationProvider = LocationManager.NETWORK_PROVIDER;
        // Or use LocationManager.GPS_PROVIDER
        //currentBestLocation = locationManager.getLastKnownLocation(locationProvider);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                Log.d(TAG, "receiving location from gps provider");
                makeUseOfNewLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this.context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // Register the listener with the Location Manager to receive location updates
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 20, 0, locationListener);
            Log.d(TAG, "Location listener installed..");
        }
        else {
            throw new ProviderException("Location provider has not permission to access location");
        }
    }

    Location currentBestLocation = null;

    private void makeUseOfNewLocation(Location location) {
        if (isBetterLocation(location , currentBestLocation)){
            String time = (currentBestLocation!=null)? new DateTime(currentBestLocation.getTime()).toDateTimeISO().toString(): DateTime.now().toString();
            Log.d(TAG, "Updating current location.. " +  time);
            currentBestLocation = location;
        }
        else {
            Log.d(TAG, "Discarding current location..");
        }
    }

    private static final int ONE_MINUTES = 1000 * 60 * 1;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > ONE_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -ONE_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            Log.d(TAG,  "Is more accurate.. update");
            return true;
        } else if (isNewer && !isLessAccurate) {
            Log.d(TAG,  "Is newer and more accurate.. update");
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            Log.d(TAG,  "Is newer and not less accurate and same provider (Most of the time) .. update");
            return true;
        }

        Log.d(TAG,  "Not updating location..");
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Override
    public DataUtils.Location getNext(DataContext context) {
        DataUtils.Location location = null;
        if (currentBestLocation == null){
            Log.d(TAG, "Giving the  provider default dummy location");
            location = new DataUtils.Location();
        }
        else {
            double latitude = currentBestLocation.getLatitude();
            double longitude = currentBestLocation.getLongitude();
            //TODO: check what to do with the time zone
            DateTime time = new DateTime(currentBestLocation.getTime());
            double altitude = currentBestLocation.getAltitude();
            double accuracy = currentBestLocation.getAccuracy();

            Log.d(TAG, "Giving the current best location");
            location = new DataUtils.Location(latitude, longitude, altitude, accuracy, time);
        }
        return location;
    }
}
