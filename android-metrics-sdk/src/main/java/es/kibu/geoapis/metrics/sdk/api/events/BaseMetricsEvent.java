package es.kibu.geoapis.metrics.sdk.api.events;

import com.google.gson.Gson;

import org.joda.time.DateTime;

import es.kibu.geoapis.serialization.GsonUtils;

/**
 * Created by lrodriguez2002cu on 06/04/2017.
 * <p>
 * <p>
 * {
 * "value": {
 * "result": true,
 * "json": "\"payload\""
 * },
 * "time": "2017-04-06T06:35:41.726Z",
 * "application": "app-45519f89e1bb4c4b",
 * "user": "user1",
 * "session": "session1",
 * "execid": "40859360-1a93-11e7-8c9c-0242ac120004",
 * "timeid": "415da3e0-1a93-11e7-8eb2-9b6442a6432d"
 * }
 */

public class BaseMetricsEvent implements MetricsEvent {

    String data;
    EventValue value;
    DateTime time;
    String application;
    String user;
    String session;
    String execid;
    String timeid;


    public BaseMetricsEvent(String data, DateTime time, String application, String user, String session, boolean result, String json, String execid, String timeid) {
        this.data = data;
        this.time = time;
        this.application = application;
        this.user = user;
        this.session = session;
        this.execid = execid;
        this.timeid = timeid;
        this.value = new EventValue(result, json);
    }


    public BaseMetricsEvent(String rawData) {
        Gson gson = GsonUtils.getGson();
        BaseMetricsEvent event = gson.fromJson(rawData, BaseMetricsEvent.class);
        this.value = event.value;
        this.time = event.time;
        this.application = event.application;
        this.user = event.user;
        this.session = event.session;
        this.execid = event.execid;
        this.timeid = event.timeid;
        this.data = rawData;
    }

    @Override
    public String getData() {
        return data;
    }

    public class EventValue {
        public boolean result;
        public String json;

        public EventValue(boolean result, String json) {
            this.result = result;
            this.json = json;
        }
    }
}
