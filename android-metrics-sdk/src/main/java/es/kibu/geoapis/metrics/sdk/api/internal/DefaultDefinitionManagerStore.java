package es.kibu.geoapis.metrics.sdk.api.internal;

import android.util.Log;

import com.activeandroid.query.Delete;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

/**
 * Created by lrodriguez2002cu on 11/01/2017.
 */
public class DefaultDefinitionManagerStore implements DefinitionManagerStore {

    public static final String TAG = "DefaultDefManagerStore";

    private static DefinitionManagerStore store;

    public static DefinitionManagerStore getStore() {
        if (store == null) {
            store = new DefaultDefinitionManagerStore();
        }
        return store;
    }

    protected DefaultDefinitionManagerStore() {
    }

    public void saveOrUpdateMetricsResults(MetricsResult data, String application) {
        Log.d(TAG, "Saving metrics definition");

        if (existsForApp(application)) {
            deleteForApp(application);
        }
        DefinitionResultRecord resultItem = DefinitionResultRecord.from(data, application);
        resultItem.save();
        Log.d(TAG, "Metrics definition saved");

    }

    public List<MetricsResult> loadForApp(String application) {

        Log.d(TAG, String.format("Loading definition for app: %s", application));

        From from = new Select().from(DefinitionResultRecord.class).where("application = ?", application);
        List<DefinitionResultRecord> results = from.execute();

        List<MetricsResult> metricsResults = new ArrayList<>();
        for (DefinitionResultRecord result : results) {
            metricsResults.add(result.asMetricsResult());
        }
        Log.d(TAG, String.format("Loaded definitions for app: %s, count: %d", application, metricsResults.size()));

        return metricsResults;
    }

    @Override
    public Map<String, MetricsResult> loadAll() {
        From from = new Select().from(DefinitionResultRecord.class)/*.where("application = ?", application)*/;
        List<DefinitionResultRecord> results = from.execute();

        Map<String, MetricsResult> metricsResults = new HashMap<>();

        for (DefinitionResultRecord result : results) {
            metricsResults.put(result.application, result.asMetricsResult());
        }
        return metricsResults;
    }

    public boolean existsForApp(String application) {
        int count = new Select().from(DefinitionResultRecord.class).where("application = ?", application).count();
        return count > 0;
    }

    public void deleteForApp(String application) {
        new Delete().from(DefinitionResultRecord.class).where("application = ?", application).execute();
    }

    @Override
    public void clearAll() {
        new Delete().from(DefinitionResultRecord.class).execute();
    }
}
