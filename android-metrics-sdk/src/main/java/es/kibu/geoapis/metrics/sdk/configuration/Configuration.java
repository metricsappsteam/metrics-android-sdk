package es.kibu.geoapis.metrics.sdk.configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 15/05/2016.
 */
public class Configuration {

    Map<String, String> configuration;

    public Configuration() {
        configuration = new HashMap<>();
    }

    public Configuration(Map<String, String> configuration) {
        this.configuration = configuration;
    }

    public void add(String property, String value) {
        configuration.put(property, value);
    }

    public boolean has(String property) {
        return configuration.containsKey(property);
    }

    public String get(String property) {
        return configuration.get(property);
    }
}
