package es.kibu.geoapis.metrics.sdk.configuration;

/**
 * Created by lrodriguez2002cu on 15/05/2016.
 */
public interface ConfigurationReader {
    Configuration readConfiguration();
}
