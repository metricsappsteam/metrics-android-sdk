package es.kibu.geoapis.metrics.sdk.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by lrodriguez2002cu on 13/01/2017.
 */

public class MetricsInstanceIdService extends FirebaseInstanceIdService {

    public static final String TAG = "MInstanceIdService";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MInstIdService", "MetricsInstanceIdService created");
/*
        You can create a topic with http api:

        https://iid.googleapis.com/iid/v1/ey6YkEV09HQ:APA91bF2gh_JM7RmuqdPMD8XJ5h0UQPyBQtZ5nMgM5sC-DHlHGLrUgYy-2zirA9Xl9kS68UBZmTPZpc75D1SqZ_MwpYvM3NIsFy1AaYWftwLCTsneES6YtSwguHpxxvTLPYIS16tB0gw/rel/topics/sample-appid1482868363997

        1. IID_TOKEN = Device registration token, you can find it with following command on your android device :
*/

        String IID_TOKEN = FirebaseInstanceId.getInstance().getToken();
        Log.d("MInstIdService", String.format("existing token: %s", IID_TOKEN));

    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String refreshedToken) {
        //// TODO: 13/01/2017  add things here in case it would be needed to have a direct messaging
        Log.d("MInstIdService", String.format("the refreshed token is: %s", refreshedToken));
    }
}
