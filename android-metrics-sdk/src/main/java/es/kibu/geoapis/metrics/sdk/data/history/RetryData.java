package es.kibu.geoapis.metrics.sdk.data.history;

import org.joda.time.DateTime;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by lrodriguez2002cu on 03/01/2017.
 */
@Table(name = "retries")
public class RetryData extends Model {

    @Column(name = "json")
    public String json;

    @Column(name = "application")
    public String application;

    @Column(name = "variable")
    public String variable;

    @Column(name = "time")
    public DateTime time;

    @Column(name = "synczed")
    public boolean synczed;

    public RetryData(String json, String application, String variable) {
        this.json = json;
        this.time = DateTime.now();
        this.synczed = false;
        this.application = application;
        this.variable = variable;
    }

    public RetryData() {
        this.time = DateTime.now();
        this.synczed = false;
    }
}

