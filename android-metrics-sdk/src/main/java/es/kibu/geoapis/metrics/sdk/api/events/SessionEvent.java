package es.kibu.geoapis.metrics.sdk.api.events;

/**
 * Created by lrodriguez2002cu on 12/01/2017.
 */
public class SessionEvent extends BaseMetricsEvent {

    public SessionEvent(String data) {
        super(data);
    }
}
