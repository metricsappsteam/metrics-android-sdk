package es.kibu.geoapis.metrics.sdk.data.providers;

/**
 * Created by lrodriguez2002cu on 07/11/2016.
 */

public class ProviderException extends RuntimeException {

    public ProviderException(String message) {
        super(message);
    }
}
