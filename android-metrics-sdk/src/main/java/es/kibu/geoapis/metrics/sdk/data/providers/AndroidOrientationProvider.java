package es.kibu.geoapis.metrics.sdk.data.providers;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.data.providers.OrientationProvider;

/**
 * Created by lrodriguez2002cu on 08/11/2016.
 */

public class AndroidOrientationProvider extends OrientationProvider implements  SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private Sensor mSensorMagnetic;
    private Context context;

    public AndroidOrientationProvider(Context context) {
        this.context = context;
        init();
    }

    int SAMPLE_PERIOD_Us = 1000000;

    private void init(){
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
        mSensorMagnetic = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        mSensorManager.registerListener(this, mSensor, SAMPLE_PERIOD_Us/*SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI*/);
        mSensorManager.registerListener(this, mSensorMagnetic, SAMPLE_PERIOD_Us/*SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI*/);
    }

    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];

    private final float[] mRotationMatrix = new float[9];
    private final float[] mOrientationAngles = new float[3];

  /*  @Override
    protected void onResume() {
        super.onResume();

        // Get updates from the accelerometer and magnetometer at a constant rate.
        // To make batch operations more efficient and reduce power consumption,
        // provide support for delaying updates to the application.
        //
        // In this example, the sensor reporting delay is small enough such that
        // the application receives an update before the system checks the sensor
        // readings again.
        mSensorManager.registerListener(this, Sensor.TYPE_ACCELEROMETER,
                SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, Sensor.TYPE_MAGNETIC_FIELD,
                SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
    }
*/

    // Get readings from accelerometer and magnetometer. To simplify calculations,
    // consider storing these readings as unit vectors.
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            System.arraycopy(event.values, 0, mAccelerometerReading,
                    0, mAccelerometerReading.length);
        }
        else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            System.arraycopy(event.values, 0, mMagnetometerReading,
                    0, mMagnetometerReading.length);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    // Compute the three orientation angles based on the most recent readings from
    // the device's accelerometer and magnetometer.
    public void updateOrientationAngles() {
        // Update rotation matrix, which is needed to update orientation angles.
        SensorManager.getRotationMatrix(mRotationMatrix, null,
                mAccelerometerReading, mMagnetometerReading);

        // "mRotationMatrix" now has up-to-date information.

        SensorManager.getOrientation(mRotationMatrix, mOrientationAngles);

        // "mOrientationAngles" now has up-to-date information.
    }


    //https://developer.android.com/reference/android/hardware/SensorManager.html#getOrientation(float[], float[])
    @Override
    public DataUtils.Orientation getNext(DataContext context) {
        DataUtils.Orientation orientation = new DataUtils.Orientation();
        orientation.z = mOrientationAngles[0];
        orientation.x = mOrientationAngles[1];
        orientation.y = mOrientationAngles[2];

        return orientation;
    }
}
