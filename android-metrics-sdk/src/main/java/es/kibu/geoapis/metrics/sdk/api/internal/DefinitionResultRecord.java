package es.kibu.geoapis.metrics.sdk.api.internal;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import org.joda.time.DateTime;

import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

/**
 * Created by lrodriguez2002cu on 11/01/2017.
 */
@Table(name = "definitions_store")
public class DefinitionResultRecord extends Model {
    @Column(name = "content")
    String content;

    @Column(name = "hash")
    String hash;

    @Column(name = "metricId")
    String metricId;

    @Column(name = "refid")
    String refid;

    @Column(name = "public")
    boolean isPublic;

    @Column(name = "time")
    public DateTime time;

    @Column(name = "application")
    public String application;


    //required default constructor
    public DefinitionResultRecord() {
    }

    public static DefinitionResultRecord from(MetricsResult result, String application) {
        return new DefinitionResultRecord(result.getContent(), result.getHash(),
                result.getMetricId(), result.getId(), result.getIsPublic(), application, DateTime.now());
    }

    public MetricsResult asMetricsResult() {
        MetricsResult mr = new MetricsResult(this.content, this.hash, this.metricId, this.isPublic);
        return mr;
    }

    public static MetricsResult toMetricsResult(DefinitionResultRecord record) {
        MetricsResult mr = record.asMetricsResult();
        return mr;
    }

    public DefinitionResultRecord(String content, String hash, String metricId, String refid, boolean isPublic, String application, DateTime time) {
        this.content = content;
        this.hash = hash;
        this.metricId = metricId;
        this.refid = refid;
        this.isPublic = isPublic;
        this.time = time;
        this.application = application;
    }
}
