package es.kibu.geoapis.metrics.sdk.api.events;

import es.kibu.geoapis.metrics.sdk.data.providers.AndroidDataSubmitter;

/**
 * Created by lrodriguez2002cu on 12/01/2017.
 */

public interface MetricsEventsReceiver {

    interface ApplicationMetricsEventsReceiver  extends MetricsEventsReceiver{
        void onMetricsApplicationEvent(ApplicationEvent event);
    }

    interface SessionMetricsEventsReceiver  extends MetricsEventsReceiver{
        void onMetricsSessionEvent(SessionEvent event);
    }

    interface UserMetricsEventsReceiver extends MetricsEventsReceiver {
        void onMetricsUserEvent(UserEvent event);
    }

    interface SubmitActionReceiver extends MetricsEventsReceiver {
        void onMetricsDataSubmitted(AndroidDataSubmitter.SubmissionDataEvent event);
    }
}
