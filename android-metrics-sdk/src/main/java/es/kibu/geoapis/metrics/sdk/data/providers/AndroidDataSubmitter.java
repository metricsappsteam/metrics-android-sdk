package es.kibu.geoapis.metrics.sdk.data.providers;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.otto.Bus;

import org.joda.time.DateTime;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.api.DataSubmitter;
import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.metrics.sdk.data.AndroidMetricsException;
import es.kibu.geoapis.metrics.sdk.data.history.RetryData;
import es.kibu.geoapis.metrics.sdk.data.history.RetryDataSaver;
import es.kibu.geoapis.metrics.sdk.data.services.AsyncResult;
import es.kibu.geoapis.metrics.sdk.data.services.DefaultMetricsAsyncServiceClient;
import es.kibu.geoapis.metrics.sdk.data.services.MetricsAsyncServiceClient;
import es.kibu.geoapis.serialization.GsonUtils;
import es.kibu.geoapis.services.objectmodel.results.SubmitResult;

/**
 * Created by lrodriguez2002cu on 10/01/2017.
 */

public class AndroidDataSubmitter implements DataSubmitter {

    public static final String RETRY_TASK = "RETRY_TASK";
    public static final String TRY_TASK = "TRY_TASK";
    public static final String APP_NOT_FOUND = "APP_NOT_FOUND";
    MetricsAsyncServiceClient client;
    Configurable config;
    Context context;
    RetryTask retryTask;
    Bus bus;

    /**
     * Creates the module based on an external client.
     *
     * @param client     the client to use in the submitter
     * @param appContext the context
     * @param config     the configuration containing data about the application and so on
     */
    public AndroidDataSubmitter(MetricsAsyncServiceClient client, Context appContext, Configurable config, Bus bus) {

        assert client != null;
        assert config != null;
        assert appContext != null;

        this.client = client;
        this.config = config;
        this.context = appContext;
        this.bus = bus;

    }

    /**
     * @param baseUrl    the base url where the services are hosted ex. "http://altairi.lan:9000";
     * @param appContext the context
     */
    public AndroidDataSubmitter(String baseUrl, Context appContext, Configurable config, Bus bus) {
        this(new DefaultMetricsAsyncServiceClient(baseUrl, appContext), appContext, config, bus);
    }

    @Override
    public void submit(Object data, DataContext context) {
        //serialize and submit
        String applicationId = config.getApplication();
        String variable = context.getVariable().getName();
        client.submitDataAsync(applicationId, variable, data,
                new SubmitterAsyncResult<SubmitResult>(data, new ContextDetails(applicationId, variable), true));

        scheduleRetry();
    }

    private void trySubmitNotSubmitted() {

        Map<RetryData, String> retryDataObjectMap = RetryDataSaver.loadAllUnSynczed();

        Log.d(TRY_TASK, String.format("To be retried = %s", retryDataObjectMap.size()));
        for (Map.Entry<RetryData, String> retryDataStringEntry : retryDataObjectMap.entrySet()) {
            RetryData data = retryDataStringEntry.getKey();
            String applicationId = data.application;
            String variable = data.variable;
            String dataContent = data.json;

            Log.d(TRY_TASK, String.format("submitting for app: %s, variable: %s", applicationId, variable));
            client.submitDataAsync(applicationId, variable, dataContent,
                    new ResubmitterAsyncResult(data));
        }
    }

    private void scheduleRetry() {
        if (retryTask == null || retryTask.getStatus() == AsyncTask.Status.FINISHED) {
            Log.d(RETRY_TASK, "Scheduling a retry task");

            retryTask = new RetryTask();
            retryTask.execute();
        } else {
            Log.d(RETRY_TASK, "Retry task is already: " + retryTask.getStatus().name());
        }
    }

    private void notify(SubmissionStage stage, ContextDetails context, Object data) {
        if (bus != null) {
            bus.post(new SubmissionDataEvent(stage, context, data));
        }
    }

    private void notify(SubmissionStage stage, ContextDetails context, Object data, Throwable failure) {
        if (bus != null) {
            bus.post(new SubmissionDataEvent(stage, context, data, failure));
        }
    }

    public enum SubmissionStage {SENT, FAILED, RETRIED, RETRIED_FAILED}

    public static class SubmissionDataEvent {

        public static final String TAG = "SubmissionDataEvent";

        public static final String LOCATION = "location";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String ALTITUDE = "altitude";
        public static final String ACCURACY = "accuracy";
        public static final String TIME = "time";

        SubmissionStage stage;
        ContextDetails context;
        Object data;
        Throwable failure;

        public SubmissionDataEvent(SubmissionStage stage, ContextDetails context, Object data, Throwable failure) {
            this.data = data;
            this.stage = stage;
            this.failure = failure;
            this.context = context;
        }

        public SubmissionDataEvent(SubmissionStage stage, ContextDetails context, Object data) {
            this(stage, context, data, null);
        }

        public ContextDetails getContext() {
            return context;
        }

        public Throwable getFailure() {
            return failure;
        }

        public SubmissionStage getStage() {
            return stage;
        }

        public Object getData() {
            return data;
        }

        public Set<String> getProperties() {
            return getInternalObject().keySet();
        }

        public Object getValue(String propertyName) {
            return getInternalObject().get(propertyName);
        }


        public boolean hasLocation() {
            try {
                getLocation();
                return true;
            } catch (AndroidMetricsException e) {
                return false;
            }
        }

        public DataUtils.Location getLocation() {
            Object location = getInternalObject().get(LOCATION);
            if (location instanceof DataUtils.Location) {
                return (DataUtils.Location) location;
            } else if (location instanceof JsonObject) {
                double latitude = ((JsonObject) location).get(LATITUDE).getAsDouble();
                double longitude = ((JsonObject) location).get(LONGITUDE).getAsDouble();
                double altitude = ((JsonObject) location).get(ALTITUDE).getAsDouble();
                double accuracy = ((JsonObject) location).get(ACCURACY).getAsDouble();
                DateTime time = null;
                try {
                    time = new DateTime(((JsonObject) location).get(TIME).getAsString());
                } catch (Exception e) {
                    Log.e(TAG, "Error while decoding time", e);
                }

                DataUtils.Location locationObj = new DataUtils.Location(latitude, longitude, altitude, accuracy, time);
                return locationObj;
            } else
                throw new AndroidMetricsException("Location not found in the properties of the data object");
        }

        private Map getInternalObject() {

            switch (getStage()) {
                case SENT:
                case FAILED:
                    if (getData() instanceof Map) {
                        return (Map) getData();
                    } else
                        throw new AndroidMetricsException("The SENT or FAILED stage event data is not correct as it is  not a map.");

                case RETRIED:
                case RETRIED_FAILED:
                    if (data instanceof RetryData) {
                        RetryData rd = (RetryData) data;
                        String json = rd.json;
                        return processAsJson(json);
                    } else if (data instanceof String) {
                        return processAsJson((String) data);
                    }
                    break;
            }
            throw new AndroidMetricsException("Stage case not covered in getInternalObject: " + getStage().name());
        }

        @NonNull
        private Map processAsJson(String json) {
            Gson gson = GsonUtils.getGson();
            JsonElement jsonElement = gson.fromJson(json, JsonElement.class);
            JsonObject asJsonObject = jsonElement.getAsJsonObject();
            return new MapWrapper(asJsonObject);
        }

        public static class MapWrapper implements Map<String, Object> {

            JsonObject object;

            public MapWrapper(JsonObject object) {
                this.object = object;
            }

            @Override
            public int size() {
                return object.size();
            }

            @Override
            public boolean isEmpty() {
                return object.size() == 0;
            }

            @Override
            public boolean containsKey(Object o) {
                return object.has((String) o);
            }

            @Override
            public boolean containsValue(Object o) {
                throw new RuntimeException("Not implemented by the wrapper");
            }

            @Override
            public Object get(Object o) {
                return object.get((String) o);
            }

            @Override
            public Object put(String s, Object o) {
                throw new RuntimeException("Not implemented by the wrapper");
            }

            @Override
            public Object remove(Object o) {
                throw new RuntimeException("Not implemented by the wrapper");
            }

            @Override
            public void putAll(@NonNull Map<? extends String, ?> map) {
                throw new RuntimeException("Not implemented by the wrapper");
            }

            @Override
            public void clear() {
                throw new RuntimeException("Not implemented by the wrapper");
            }

            @NonNull
            @Override
            public Set<String> keySet() {
                Set<String> stringSet = new HashSet<>();
                for (Entry<String, JsonElement> entry : object.entrySet()) {
                    stringSet.add(entry.getKey());
                }
                return stringSet;
            }

            @NonNull
            @Override
            public Collection<Object> values() {
                Set<Object> stringSet = new HashSet<>();
                for (Entry<String, JsonElement> entry : object.entrySet()) {
                    stringSet.add(entry.getValue());
                }
                return stringSet;
            }

            @NonNull
            @Override
            public Set<Entry<String, Object>> entrySet() {
                Set<Entry<String, Object>> entries = new HashSet<>();
                for (final Entry<String, JsonElement> entry : object.entrySet()) {
                    entries.add(new Entry<String, Object>() {
                        @Override
                        public String getKey() {
                            return entry.getKey();
                        }

                        @Override
                        public Object getValue() {
                            return entry.getValue();
                        }

                        @Override
                        public Object setValue(Object o) {
                            throw new RuntimeException("Cant modify this ");
                        }
                    });
                }

                return entries;
            }
        }
    }

    public static class DummySubmitter implements DataSubmitter {

        public static final String TAG = "DummySubmitter";

        @Override
        public void submit(Object data, DataContext context) {
            Log.d(TAG, "submit: " + data.toString());
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

    public static class ContextDetails {
        String application;
        String variable;

        public ContextDetails(String application, String variable) {
            this.application = application;
            this.variable = variable;
        }
    }

    private class RetryTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            trySubmitNotSubmitted();
            return null;
        }
    }

    protected class SubmitterAsyncResult<Result> extends AsyncResult<Result> {

        public static final String SUBMIT_ERROR = "SUBMIT_ERROR";

        Object data;
        boolean saveOnFailure;
        ContextDetails contextDetails;
        Gson gson = GsonUtils.getGson();

        /**
         * Async result in charge of notifying regarding the submit operation
         *
         * @param data the data to be submitted or stored later if something goes wrong.
         */
        public SubmitterAsyncResult(Object data, ContextDetails contextDetails, boolean saveOnFailure) {
            super(null);
            this.data = data;
            this.saveOnFailure = saveOnFailure;
            this.contextDetails = contextDetails;

            assert contextDetails.application != null;
            assert !contextDetails.application.isEmpty();
            assert !contextDetails.variable.isEmpty();
            assert contextDetails.variable != null;
        }


        @Override
        public void onFailure(Exception ex) {
            Log.d(SUBMIT_ERROR, (ex.getMessage() != null) ? ex.getMessage() : ex.getClass().getName());

            //Will save to history in case  it fails to send.
            if (saveOnFailure) {
                if (isAppNotFoundError(ex)) {
                    Log.d(SUBMIT_ERROR, String.format("The app responded with (%s) so no retry will be done in the future", ex.getMessage()));
                }
                boolean synczed = isAppNotFoundError(ex);

                RetryDataSaver.saveResults(gson.toJson(data), contextDetails.application, contextDetails.variable, synczed);
            }
            AndroidDataSubmitter.this.notify(SubmissionStage.FAILED, this.contextDetails, data, ex);
        }

        //This method tells if the error was APP_NOT_FOUND
        private boolean isAppNotFoundError(Exception ex) {
            return ex.getMessage() != null && ex.getMessage().startsWith(APP_NOT_FOUND);
        }

        @Override
        public void onSuccess(Result result) {
            Log.d("SUBMIT_SUCCESS", (data.getClass().getSimpleName() + " Object:" + result.toString()));
            AndroidDataSubmitter.this.notify(SubmissionStage.SENT, this.contextDetails, data);
        }
    }

    private class ResubmitterAsyncResult<Result> extends AsyncResult<Result> {
        public static final String TAG = "RESUBMIT_ERROR";
        long id;
        RetryData data;

        /**
         * Async result in charge of notifying regarding the submit operation
         *
         * @param data the id to be updated later if something goes well.
         */

        public ResubmitterAsyncResult(RetryData data) {
            super(null);
            this.id = data.getId();
            this.data = data;
        }

        @Override
        public void onFailure(Exception ex) {
            Log.d(TAG, (ex.getMessage() != null) ? ex.getMessage() : ex.getClass().getName());

            if (ex.getMessage() != null && ex.getMessage().startsWith(APP_NOT_FOUND)) {
                Log.d(TAG, String.format("The app responded with (%s) so no retry will be done in the future", ex.getMessage()));
                //In the case the app was already deleted, which could be the case..
                //update  it so that it is no longer considered.
                RetryDataSaver.updateSyncsed(data);
            }

            AndroidDataSubmitter.this.notify(SubmissionStage.RETRIED_FAILED, new ContextDetails(data.application, data.variable), data, ex);
        }

        @Override
        public void onSuccess(Result result) {
            Log.d("RESUBMIT_SUCCESS", result.toString());
            RetryData data = RetryData.load(RetryData.class, id);
            //Will save to history in case  it fails to send.
            RetryDataSaver.updateSyncsed(data);
            AndroidDataSubmitter.this.notify(SubmissionStage.RETRIED, new ContextDetails(data.application, data.variable), data);
        }
    }
}
