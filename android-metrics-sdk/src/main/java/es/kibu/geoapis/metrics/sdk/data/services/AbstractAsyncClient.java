package es.kibu.geoapis.metrics.sdk.data.services;

import android.os.AsyncTask;

public class AbstractAsyncClient {

	private ProgressHandler handler;
	protected AbstractServiceClient client;

	public AbstractAsyncClient() {
		super();
	}

	protected void setHandler(ProgressHandler handler) {
		this.handler = handler;
		if (client != null) {
			client.setHandler(this.handler);
		}
	}

	protected ProgressHandler getHandler() {
		return handler;
	}

	/**
	 * Object for notifying the progress of the operations.
	 * 
	 * @author lrodriguez
	 * 
	 */

	class ProgressObject {
		private final String message;
		private final Object associatedObject;
		private final int progress;

		public ProgressObject(String message, Object associatedObject,
							  int progress) {
			this.message = message;
			this.associatedObject = associatedObject;
			this.progress = progress;
		}
	}

	public abstract class InternalAsyncTask<Result> extends
			AsyncTask<Void, ProgressObject, AsyncResult<Result>> {

		private final ProgressHandler methodHandler;
		private final AsyncResult<Result> asyncResult;

		@Override
		protected void onProgressUpdate(ProgressObject... progress) {
			ProgressObject progressObj = progress[0];
			if (methodHandler != null) {
				methodHandler.notifyProgress(progressObj.message,
						progressObj.associatedObject, progressObj.progress);
			}
		}

		@Override
		protected void onPostExecute(AsyncResult<Result> result) {
			if (result.isFailed()) {
				result.onFailure(result.getFailureException());
			} else {
				result.onSuccess(result.getResult());
			}
		}

		public void publishProgress(ProgressObject progress) {
			super.publishProgress(progress);
		}

		public InternalAsyncTask(AsyncResult<Result> asyncResult) {
			this.methodHandler = asyncResult.getProgressHandler();
			this.asyncResult = asyncResult;
		}

		public ProgressHandler getMethodHandler() {
			return methodHandler;
		}

		public AsyncResult<Result> getAsyncResult() {
			return asyncResult;
		}

		@Override
		protected AsyncResult<Result> doInBackground(Void... params) {
			AbstractAsyncClient.this.setHandler(new AsyncProgressHandler(this));
			try {
				Result methodResult = callBackgroundMethod();
				asyncResult.setSuccessResult(methodResult);
			} catch (Exception e) {
				asyncResult.setFailureResult(e);
			}
			return asyncResult;
		}

		public void execute() {
			this.execute((Void[]) null);
		}

		public abstract Result callBackgroundMethod() throws Exception;
	}

    class AsyncProgressHandler implements ProgressHandler {
		InternalAsyncTask<?> task;

		public void notifyProgress(String message, Object associatedObject,
								   int progress) {
			task.publishProgress(new ProgressObject(message, associatedObject,
					progress));

		}

		public AsyncProgressHandler(InternalAsyncTask<?> task) {
			this.task = task;
		}
	}

}