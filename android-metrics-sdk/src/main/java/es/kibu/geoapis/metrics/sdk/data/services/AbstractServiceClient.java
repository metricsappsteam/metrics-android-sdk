package es.kibu.geoapis.metrics.sdk.data.services;


public class AbstractServiceClient {

	protected ProgressHandler handler = null;

	public AbstractServiceClient() {
		super();
	}

	public ProgressHandler getHandler() {
		return handler;
	}

	public void setHandler(ProgressHandler handler) {
		this.handler = handler;
	}

}