package es.kibu.geoapis.metrics.sdk.api.internal;

import java.util.List;
import java.util.Map;

import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

/**
 * Created by lrodriguez2002cu on 11/01/2017.
 */
public interface DefinitionManagerStore {

    void saveOrUpdateMetricsResults(MetricsResult data, String application);

    List<MetricsResult> loadForApp(String application);

    Map<String, MetricsResult> loadAll();

    boolean existsForApp(String application);

    void deleteForApp(String application);

    void clearAll();

}
