package es.kibu.geoapis.metrics.sdk.data.services;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.services.objectmodel.results.CreateResult;
import es.kibu.geoapis.services.objectmodel.results.DeleteResult;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;
import es.kibu.geoapis.services.objectmodel.results.SubmitResult;
import es.kibu.geoapis.services.objectmodel.results.UpdateResult;

public class DefaultMetricsAsyncServiceClient extends AbstractAsyncClient implements MetricsAsyncServiceClient {

    /*
    GET         /api/v1/metrics/:appid                      controllers.Application.metrics(appid: String)
    GET         /api/v1/metrics-exist/:appid                controllers.Application.metricsExist(appid: String)
    GET         /api/v1/test/:appid                         controllers.Application.test(appid: String)

    POST        /api/v1/update-metrics/:appid               controllers.Application.updateMetrics(appid: String)
    POST        /api/v1/create-metrics/:appid               controllers.Application.createMetrics(appid: String)
    POST        /api/v1/delete-metrics/:appid               controllers.Application.deleteMetrics(appid: String)

    POST        /api/v1/submit-data/:appid/:variable        controllers.Application.submitData(appid: String, variable: String)
    */

    public static final String API_V1 = "/api/v1/";

    static String getServiceURL(String path) {
        return API_V1 + path.trim();
    }
    public static final String URL_METRICS = getServiceURL("metrics/:appid");   //        controllers.Application.metrics(appid: String)
    public static final String URL_METRICS_EXIST = getServiceURL("metrics-exist/:appid ");   //       controllers.Application.metricsExist(appid: String)
    public static final String URL_TEST = getServiceURL("test/:appid"); //       controllers.Application.test(appid: String)  public static final string  URL
    public static final String URL_UPDATE_METRICS = getServiceURL("update-metrics/:appid");    //       controllers.Application.updateMetrics(appid: String)
    public static final String URL_CREATE_METRICS = getServiceURL("create-metrics/:appid");    //       controllers.Application.createMetrics(appid: String)
    public static final String URL_DELETE_METRICS = getServiceURL("delete-metrics/:appid");    //       controllers.Application.deleteMetrics(appid: String)  public static final string  URL

    public static final String URL_SUBMIT_DATA = getServiceURL("submit-data/:appid/:variable");    //    controllers.Application.submitData(appid: String, variable: String)

    interface QueueHandler {
        void createHandler(Context context);
        Request addToQueue(Request request);
    }

    private String baseUrl;
    private QueueHandler handler;

    class DefaultVolleyQueueHandler implements QueueHandler{

        private RequestQueue mRequestQueue;

        public DefaultVolleyQueueHandler(Context context) {
            createHandler(context);
        }

        public DefaultVolleyQueueHandler(){

        }

        @Override
        public void createHandler(Context context) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        @Override
        public Request addToQueue(Request request) {
            return mRequestQueue.add(request);
        }
    }

    private static String withApplicationId(String URL, String applicationId) {
        String template = ":appid";
        String value = applicationId;
        return fixUrl(URL, template, value);
    }

    private static String withVariable(String URL, String variable) {
        String template = ":variable";
        String value = variable;
        return fixUrl(URL, template, value);
    }

    private static String fixUrl(String URL, String template, String value) {
        return URL.replaceFirst(template, value);
    }

    public DefaultMetricsAsyncServiceClient(String baseUrl, Context context) {
        handler = new DefaultVolleyQueueHandler(context);
        this.baseUrl = baseUrl;
    }

    String getURL(String path) {
        if (path.contains(":")) throw new RuntimeException("Raw URL used: '" + path + "'");
        String sep = path.startsWith("/") ? "" : "/";
        return baseUrl + sep + path;
    }

    private <T> Response.Listener<T> getListener(final AsyncResult<T> asyncResult) {
        return new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                asyncResult.setSuccessResult(response);
                asyncResult.onSuccess(response);
            }
        };
    }

    Response.ErrorListener getErrorListener(final AsyncResult asyncResult) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Exception cause = error;
                if (error.getCause() instanceof Exception) {
                    cause = (Exception) error.getCause();
                }
                asyncResult.setFailureResult(cause);
                asyncResult.onFailure(cause);

            }
        };
    }

    @Override
    public void metricsAsync(String applicationId, AsyncResult<MetricsResult> result) {
        String urlMetrics = withApplicationId(URL_METRICS, applicationId);
        GsonRequest request = GsonRequest.get(getURL(urlMetrics), MetricsResult.class, null, getListener(result), getErrorListener(result));
        addToQueue(request);
    }

    @Override
    public void metricsExistAsync(String applicationId, AsyncResult<ExistResult> result) {
        String urlMetricsExist = withApplicationId(URL_METRICS_EXIST, applicationId);
        GsonRequest request = GsonRequest.get(
                getURL(urlMetricsExist), ExistResult.class, null, getListener(result), getErrorListener(result));
        addToQueue(request);
    }

    private Request addToQueue(GsonRequest request) {
        //TODO: Can this deserializer be only one instance?
        request.withDeserializer(new DefaultResponseDeserializer());
        return handler.addToQueue(request);
    }


    @Override
    public void createMetricAsync(String applicationId, MetricsDefinition definition, AsyncResult<CreateResult> result) {
        String urlCreateMetrics = withApplicationId(URL_CREATE_METRICS, applicationId);
        GsonRequest request = GsonRequest.post(getURL(urlCreateMetrics),
                CreateResult.class, definition, null, getListener(result), getErrorListener(result));
        addToQueue(request);
    }

    @Override
    public void submitDataAsync(String applicationId, String variable, Object data, AsyncResult<SubmitResult> result) {
        String urlCreateMetrics = withApplicationId(URL_SUBMIT_DATA, applicationId);
        urlCreateMetrics = withVariable(urlCreateMetrics, variable);
        GsonRequest request = GsonRequest.post(getURL(urlCreateMetrics),
                SubmitResult.class, data, null, getListener(result), getErrorListener(result));
        addToQueue(request);
    }

    @Override
    public void deleteMetricsAsync(String applicationId, AsyncResult<DeleteResult> result) {
        String urlPath = withApplicationId(URL_DELETE_METRICS, applicationId);
        String url = getURL(urlPath);
        GsonRequest request = GsonRequest.post(url, DeleteResult.class, null, getListener(result), getErrorListener(result));
        addToQueue(request);
    }

    @Override
    public void updateMetricsAsync(String applicationId, MetricsDefinition definition, AsyncResult<UpdateResult> result) {
        String urlUpdate = withApplicationId(URL_UPDATE_METRICS, applicationId);
        GsonRequest request = GsonRequest.post(getURL(urlUpdate), UpdateResult.class, definition, null, getListener(result), getErrorListener(result));
        addToQueue(request);
    }

}
