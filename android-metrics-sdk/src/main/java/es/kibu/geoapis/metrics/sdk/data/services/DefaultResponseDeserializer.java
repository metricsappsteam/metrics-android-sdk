package es.kibu.geoapis.metrics.sdk.data.services;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.lang.reflect.Type;

import es.kibu.geoapis.services.objectmodel.results.ExistResult;
import es.kibu.geoapis.services.objectmodel.results.ResultValue;
import es.kibu.geoapis.services.objectmodel.results.ResultValueReader;

/**
 * Created by lrodriguez2002cu on 04/01/2017.
 */

public class DefaultResponseDeserializer implements GsonRequest.ResponseDeserializer {

    public static final String TAG = "RESPONSE_DESERIALIZER";

    public class ServiceException extends Exception{

        public ServiceException(String message) {
            super(message);
        }
    }
    public Exception getResponseException(String json, Class clazz) {
        try {
            ResultValue<Object> objectResultValue = ResultValueReader.asResult(json, clazz);
            return new ServiceException(objectResultValue.getErrorMessage());
        } catch (Exception e) {
            Log.e(TAG, "Error while deserializing: "+ json, e);
        }
        return new ServiceException("Unrecognized error value:" + json);
    }

    public <T> T getResponseDeserialized(String json, Class<?> clazz)  {
        try {
            ResultValue<T> objectResultValue = ResultValueReader.asResult(json, clazz);
            return objectResultValue.getResult();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
