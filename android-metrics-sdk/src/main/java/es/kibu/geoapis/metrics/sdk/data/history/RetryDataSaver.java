package es.kibu.geoapis.metrics.sdk.data.history;

import android.util.Log;

import com.activeandroid.query.Delete;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 03/01/2017.
 */

public class RetryDataSaver {


    public static final String TAG = "RetryDataSaver";

    public static <T> void saveResults(T data, String application, String variable, boolean synczed) {
        Log.d(TAG, "Saving results to database temporarily");
        String json = data.toString();
        RetryData resultItem = new RetryData(json, application, variable);
        resultItem.synczed = synczed;
        resultItem.save();
    }

    public static <T> Map<RetryData, T> loadAll() {
        return loadAll(null);
    }

    public static void clearHistoryResults() {
        new Delete().from(RetryData.class).execute();
    }

    public static void updateHistory(RetryData retryData, boolean syncsed) {
        retryData.synczed = syncsed;
        retryData.save();
    }

    public static void updateSyncsed(RetryData retryData) {
        retryData.synczed = true;
        retryData.save();
    }

    public static <T> Map<RetryData, T> loadAllSynczed() {
        return loadAll(true);
    }

    public static <T> Map<RetryData, T> loadAllUnSynczed() {
        return loadAll(false);
    }

    public static <T> Map<RetryData, T> loadAll(Boolean syncsed) {

        From from = (syncsed != null) ? new Select()
                .from(RetryData.class).where("synczed = ?", syncsed) : new Select()
                .from(RetryData.class);

        List<RetryData> results = from.execute();

        Map<RetryData, T> answers = new HashMap<>();

        Log.d(TAG, String.format("Found %s sends to retry", results.size()));
        for (RetryData result : results) {
            String json = result.json;
            T answer = testAnswersFromString(json);
            answers.put(result, answer);
        }
        return answers;
    }


    //TODO: this  MUST be implemented
    public static <T> T testAnswersFromString(String json) {
        T answer = (T) json;
        //ObjectMapper mapper = SerializationUtils.getConfiguredObjectMapper();
          /*  Gson gson = SerializationUtils.getPreparedGson();//new Gson();
            TestAnswers answer = null;
            answer = gson.fromJson(json, TestAnswers.class);
*/
        if (deserializer != null) {
            answer = deserializer.getAs(json);
        }
        return answer;
    }

    interface HistoryDeserializer<T> {
        <T> T getAs(String json);
    }

    static HistoryDeserializer<?> deserializer;

    public static <T> void registerDeserializer(HistoryDeserializer<T> deserializer) {
        RetryDataSaver.deserializer = deserializer;
    }

    public static void clearRetryData(){
        From from = new Delete()
                .from(RetryData.class);
        from.execute();
    }
}
