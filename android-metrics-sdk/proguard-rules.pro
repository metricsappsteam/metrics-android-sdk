# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\installs\android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes EnclosingMethod

#-keep public class es.kibu.geoapis.metrics.sdk.api.MetricsApi {
#   public protected *;
#}
#
#-keep public class es.kibu.geoapis.metrics.sdk.api.MetricsApiException {
#    public protected *;
# }
#
#-keep public class es.kibu.geoapis.metrics.sdk.api.MetricsConfig {
#   public *;
#}

#-dontobfuscate
#-keepattributes *Annotation*
#
#-keep public class * extends android.app.Activity
#-keep public class * extends android.app.Application
#-keep public class * extends android.app.Service
#-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends android.content.ContentProvider
#-keep public class * extends com.google.android.gms.gcm.GcmListenerService
#-keep public class * extends com.google.android.gms.iid.InstanceIDListenerService
#
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet);
#}
#
#-keepclasseswithmembers class * extends com.activeandroid.Model
#
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#}
#
#-keepclassmembers class * implements android.os.Parcelable {
#    static android.os.Parcelable$Creator CREATOR;
#}
#
#-keepclassmembers class **.R$* {
#    public static <fields>;
#}
#
#-keep public class es.kibu.geoapis.metrics.sdk.** {
#    public protected *;
#    public static <fields>;
#}
#
#-keepclasseswithmembers public class *.activeandroid.**
#-keepclasseswithmembers public class com.activeandroid.TableInfo
#-keepclasseswithmembers public class com.activeandroid.ModelInfo
#
##com.activeandroid.TableInfo com.activeandroid.ModelInfo
#-keepclasseswithmembers public class es.kibu.geoapis.metrics.sdk.** {
#    *;
#}

-keepattributes *Annotation*
-keepclassmembers class ** {
    @com.squareup.otto.Subscribe public *;
    @com.squareup.otto.Produce public *;
}

