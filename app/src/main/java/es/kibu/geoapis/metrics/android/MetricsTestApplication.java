package es.kibu.geoapis.metrics.android;

import android.app.Application;

import es.kibu.geoapis.metrics.sdk.api.AndroidMetricsApi;
import es.kibu.geoapis.metrics.sdk.api.MetricsConfig;

/**
 * Created by lrodriguez2002cu on 11/01/2017.
 */

public class MetricsTestApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        final String application = "app-2472261933754106";
        final String user = "user1";
        final String session = "session6";

        AndroidMetricsApi.init(this, new MetricsConfig() {
            public String getUser() {
                return user;
            }

            public String getApplication() {
                return application;
            }

            public String getSession() {
                return session;
            }
        });
    }
}
