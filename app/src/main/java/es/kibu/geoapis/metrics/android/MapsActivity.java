package es.kibu.geoapis.metrics.android;

import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

import java.util.Map;

import es.kibu.geoapis.api.Tracker;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.metrics.sdk.api.AndroidMetricsApi;
import es.kibu.geoapis.metrics.sdk.api.events.ApplicationEvent;
import es.kibu.geoapis.metrics.sdk.api.events.MetricsEventsReceiver;
import es.kibu.geoapis.metrics.sdk.api.events.SessionEvent;
import es.kibu.geoapis.metrics.sdk.api.events.UserEvent;
import es.kibu.geoapis.metrics.sdk.data.providers.AndroidDataSubmitter;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        MetricsEventsReceiver.SubmitActionReceiver,
        MetricsEventsReceiver.ApplicationMetricsEventsReceiver,
        MetricsEventsReceiver.SessionMetricsEventsReceiver,
        MetricsEventsReceiver.UserMetricsEventsReceiver
{

    private GoogleMap mMap;
    Tracker defaultTracker;

    public static final int PERMISSIONS_REQUEST = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        findViewById(R.id.sendBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(MapsActivity.this,
                        ACCESS_FINE_LOCATION);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MapsActivity.this,
                            new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, INTERNET, ACCESS_NETWORK_STATE},
                            PERMISSIONS_REQUEST
                            );
                }
                else {
                    sendMovement();
                }

            }
        });

        AndroidMetricsApi.registerReceiver(this);

    }

    private void sendMovement() {
        if (defaultTracker== null) {
            defaultTracker = AndroidMetricsApi.getInstance().getDefaultTracker();
        }
        defaultTracker.send("movement", "steps", 1);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    sendMovement();

                } else {

                    Log.e("METRICS_SDK", "Permisions were not granted");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Subscribe
    @Override
    public void onMetricsDataSubmitted(AndroidDataSubmitter.SubmissionDataEvent event) {
        Object data = event.getData();
        Toast.makeText(this, String.format("%s: %s", event.getStage().name(), data), Toast.LENGTH_SHORT).show();

        if (event.hasLocation()) {
            DataUtils.Location location = event.getLocation();
            if (!location.isZero()) {
                LatLng lastMapped = new LatLng(location.latitude, location.longitude);
                mMap.addMarker(new MarkerOptions().position(lastMapped));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(lastMapped));
            }
        }

    }

    @Subscribe
    @Override
    public void onMetricsApplicationEvent(ApplicationEvent event) {
        Toast.makeText(this, String.format("App event received! data: %s",
                event.getData()), Toast.LENGTH_SHORT).show();
    }
    @Subscribe
    @Override
    public void onMetricsSessionEvent(SessionEvent event) {
        Toast.makeText(this, String.format("Session event received! data: %s",
                event.getData()), Toast.LENGTH_SHORT).show();
    }
    @Subscribe
    @Override
    public void onMetricsUserEvent(UserEvent event) {
        Toast.makeText(this, String.format("User event received! data: %s",
                event.getData()), Toast.LENGTH_SHORT).show();
    }
}
